#pragma once

//
// Ignore deprecated functions
//
#pragma warning( disable : 4996 )

//
// Simple assert macro
//
#include <cassert>
#include <iostream>

void my_assert(bool condition, const char* expression, const char* message, ...);

#define ASSERT(c) my_assert(!!(c), #c, "No message, please check expression.")
#define ASSERT2(c, msg, ...) my_assert(!!(c), #c, msg, __VA_ARGS__)
#define ASSERT_NUMBER(v) ASSERT2(v == v, "Not a valid number.")

//
// Basic typedefs
//
typedef unsigned int uint;

//
// Disallow copy macro
//
#define DISALLOW_COPY(class_name) \
    class_name(const class_name&);   \
    void operator=(const class_name&)

//
// Debug print formatted
//
// #define VERBOSE_DEBUG
#ifdef VERBOSE_DEBUG
#define DPF(f, ...) printf("> " f "\n", __VA_ARGS__)
#else
#define DPF(f, ...)
#endif

//#define GENETIC_DEBUG

#define STRINGIFYX(x) #x
#define STRINGIFY(x) STRINGIFYX(x)
