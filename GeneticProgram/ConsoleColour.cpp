#include "Common.h"
#include "ConsoleColour.h"

#include<windows.h>

int Console::GetForegroundColour()
{
    CONSOLE_SCREEN_BUFFER_INFO buffer_info;
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(console, &buffer_info);
    int a = buffer_info.wAttributes;
    return a % 16;
}

int Console::GetBackgroundColour()
{
    CONSOLE_SCREEN_BUFFER_INFO buffer_info;
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(console, &buffer_info);
    int a = buffer_info.wAttributes;
    return ( a / 16 ) % 16;
}

void Console::SetColour(ConsoleColour::Type foreground, ConsoleColour::Type background)
{
    SetColour(static_cast<int>(foreground), static_cast<int>(background));
}

void Console::SetColour(int foreground, int background)
{
    foreground %= 16;
    background %= 16;
    unsigned short attributes =   static_cast<unsigned short>(background << 4)
                                | static_cast<unsigned short>(foreground);
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

    SetConsoleTextAttribute( console, attributes );
}

std::ostream& operator << (std::ostream& stream, ConsoleColour::Type colour)
{
    stream.flush();
    Console::SetColour(colour, Console::GetBackgroundColour());
    return stream;
}
