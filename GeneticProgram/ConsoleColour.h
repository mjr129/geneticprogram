#pragma once

#include <ostream>

struct ConsoleColour
{
    enum Type
    {
        Black,
        DarkBlue,
        DarkGreen,
        DarkCyan,
        DarkRed,
        DarkPurple,
        DarkYellow,
        LightGray,
        DarkGray,
        Blue,
        Green,
        Cyan,
        Red,
        Magenta,
        Yellow,
        White,
        Reset = LightGray,

        Iterations = White,
        StoppingCondition = Yellow,
    };
};

namespace Console
{
    void SetColour(ConsoleColour::Type foreground, ConsoleColour::Type background);
    void SetColour(int foreground, int background);
    int GetForegroundColour();
    int GetBackgroundColour();
}

std::ostream& operator << (std::ostream& stream, ConsoleColour::Type colour);