#include "Common.h"
#include "DataSet.h"
#include "Utilities.h"
#include <fstream>
#include <string>
#include <sstream>

void DataSet::LoadSet(const DataSet& source, std::vector<int> indices)
{
    std::stringstream ss;
    ss << "SAMPLE(n = " << indices.size() << "):";

    this->observations.clear();

    for(auto var : indices)
    {
        this->observations.push_back(source.observations[var]);
        ss << " " << var;
    }

    title = ss.str();
}

///----------------------------------------------------------------------------
///                                                                            
/// Reads DataSet from a file                                                  
///                                                                            
/// File format is space or tab separated data.                                
/// Each data set goes on a new line.                                          
/// Either of CrLf or Lf are accepted.                                         
///                                                                            
///----------------------------------------------------------------------------
void DataSet::LoadFile(const std::string& file_name)
{
    const char* tokens = " \t\r,";

    title = file_name;
    this->observations.clear();

    if (file_name.size() == 0)
    {
        return;
    }

    std::ifstream data_file( file_name, std::ifstream::in);
    std::string in_line;

    while (std::getline(data_file, in_line))
    {
        std::cout << "\rLoading..." << "|/-\\|/-\\"[this->observations.size() % 8];
        size_t cursor = 0;

        if (in_line.size() != 0)
        {
            this->observations.push_back(Observation());

            std::string token;
            
            if (Utilities::Tokenise(in_line, tokens, cursor, token))
            {
                double value = atof(token.c_str());
                ASSERT_NUMBER(value);
                this->observations.back().expected = value;
            }

            while(Utilities::Tokenise(in_line, tokens, cursor, token))
            {
                double value = atof(token.c_str());
                ASSERT_NUMBER(value);
                this->observations.back().data.push_back(value);
            }

            if (this->observations.size() != 1)
            {
                int size_of_this = static_cast<int>(this->observations[this->observations.size() - 1].data.size());
                int size_of_previous = static_cast<int>(this->observations[this->observations.size() - 2].data.size());
                ASSERT2(size_of_this == size_of_previous, "Observations should contain the same number of data points (%d / %d on line %d).", size_of_this, size_of_previous, this->observations.size());
            }
        }
    }

    std::cout << "\r           \r";

    data_file.close();
}

int DataSet::variable_count() const
{
    if (observations.size() == 0)
    {
        return 0;
    }

    return static_cast<int>(observations[0].data.size()); 
}