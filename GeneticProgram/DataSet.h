#pragma once

#include <vector>

///
/// Represents an observation within data set
///
class Observation
{
public:
    /// The expected class or value of the data
    double expected;

    /// The values for the data
    std::vector<double> data;

    /// Constructor
    Observation() : expected(0) { }
};

class DataSet
{
public:
    std::string title;
    std::vector<Observation> observations;
    void LoadFile(const std::string& file_name);
    void LoadSet(const DataSet& source, std::vector<int> indices);

public:
    int variable_count() const;
};