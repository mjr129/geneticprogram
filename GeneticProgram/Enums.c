#include "Common.h"

#undef ENUM_BEGIN
#undef ENUM
#undef ENUM_END

#define ENUM_BEGIN(name) const char* name::Names[] = {
#define ENUM(value) #value,
#define ENUM_END };

#include "Environment.h"
