#include "Enums.h"

#undef BEGIN_ENUM
#undef ENUM_VALUE
#undef END_ENUM

#define BEGIN_ENUM(enum_name)               \
    const char* enum_name##::Names[] = {    \

#define ENUM_VALUE(enum_value)          \
        #enum_value ,

#define END_ENUM(enum_name)             \
        };                              \
        const int enum_name##::Length = (sizeof( enum_name##::Names ) / sizeof(const char*));

#undef ENUMS_H
#define ENUMS_CPP
#include "Enums.h"
