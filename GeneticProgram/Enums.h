#ifndef ENUMS_H
#define ENUMS_H

///-----------------------------------------------------------------------------
///                                                                             
/// Enum macro to allow fast enum<-->string conversion.                         
///                                                                             
///-----------------------------------------------------------------------------
#ifndef ENUMS_CPP
#define BEGIN_ENUM(enum_name)           \
    struct enum_name                    \
    {                                   \
        static const char* Names[];     \
        static const int Length;        \
        enum Type                       \
        {                               

#define ENUM_VALUE(enum_value)          \
        enum_value,

#define END_ENUM(enum_name)             \
        };                              \
   };
#endif

///-----------------------------------------------------------------------------
///                                                                             
/// Stopping conditon reason reports                                            
///                                                                             
///-----------------------------------------------------------------------------
BEGIN_ENUM(StopReason)
    ENUM_VALUE(None)            /// < Simulation not stopped
    ENUM_VALUE(Generation)      /// < Maximum generation reached
    ENUM_VALUE(Unchanging)      /// < Fitness has not improved over generations
    ENUM_VALUE(Matched)         /// < Maximum training data matches reached
    ENUM_VALUE(TwoStage1)       /// < Two-stage stage-one over but not stopped
END_ENUM(StopReason)

///-----------------------------------------------------------------------------
///                                                                             
/// Which stage of processing the training has reached                          
///                                                                             
///-----------------------------------------------------------------------------
BEGIN_ENUM(ProcessingStage)
    ENUM_VALUE(normal)
    ENUM_VALUE(second)
END_ENUM(ProcessingStage)

///-----------------------------------------------------------------------------
///                                                                             
/// How fitness is calculated                                                   
///                                                                             
///-----------------------------------------------------------------------------
BEGIN_ENUM(FitnessFunction)
    ENUM_VALUE(classifier)      /// sum( ((int)result == (int)expected) ? 0 : 1)
                                /// This appears to be the method used in Davis's program
                                /// Fitness will be reported to the user as "% matched".
    ENUM_VALUE(difference)      /// sum( abs( result - expected ) )
                                /// A method more suitible for continuous data.
                                /// Fitness will be reported to the user as "average discrepancy".
END_ENUM(FitnessFunction)

///-----------------------------------------------------------------------------
///                                                                             
/// How programs are selected for breeding                                      
///                                                                             
///-----------------------------------------------------------------------------
BEGIN_ENUM(SelectionMethod)
    ENUM_VALUE(rank)            /// Chance of being selected is proportional to rank
                                /// This appears to be the method used in Davis's program
    ENUM_VALUE(fitness)         /// Chance of being selected is proportional to adjusted fitness.
END_ENUM(SelectionMethod)

///-----------------------------------------------------------------------------
///                                                                             
/// What mode this application runs in                                          
///                                                                             
///-----------------------------------------------------------------------------
BEGIN_ENUM(ProcessingMode)
    ENUM_VALUE(train)           /// Training simulation.
    ENUM_VALUE(evaluate)        /// Program evaluation.
END_ENUM(ProcessingMode)

#endif
