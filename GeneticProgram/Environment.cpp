#include "Common.h"
#include "Environment.h"
#include "FunctionOperator.h"
#include "Randomiser.h"
#include "FileWriter.h"
#include "OperatorBag.h"
#include "Program.h"
#include "Utilities.h"
#include "World.h"
#include "ProcessResult.h"
#include "ConsoleColour.h"

#include <ostream>
#include <ctime>
#include <cstdarg>
#include <sstream>
#include <map>

///-----------------------------------------------------------------------------
///                                                                             
/// Destructor                                                                  
///                                                                             
///-----------------------------------------------------------------------------
Environment::~Environment()
{
    delete _out;
    delete _randomiser;
    delete _program;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Returns if a variable has changed from its last stage                       
/// Updates the last state if this is the case                                  
///                                                                             
///-----------------------------------------------------------------------------
bool HasChanged(std::string& current, std::string& last)
{
    if (current != last)
    {
        last = current;
        return true;
    }

    return false;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Makes sure all data is loaded                                               
///                                                                             
/// Also cleans up any leftovers from the two-stage process                     
///                                                                             
///-----------------------------------------------------------------------------
void Environment::InitialiseStage1()
{
    //
    // Calculate absolutes
    //
    replication_breeding_quantity   = static_cast<int>(replication_breeding  * population_size + 0.5);
    crossover_breeding_quantity     = static_cast<int>(crossover_breeding    * population_size + 0.5);
    mutation_breeding_quantity      = static_cast<int>(mutation_breeding     * population_size + 0.5);

    if ((replication_breeding_quantity + crossover_breeding_quantity + mutation_breeding_quantity) > population_size)
    {
        crossover_breeding_quantity -= 1;
    }
    if ((crossover_breeding_quantity % 2) != 0)
    {
        crossover_breeding_quantity++;
    }
    if ((replication_breeding_quantity + crossover_breeding_quantity + mutation_breeding_quantity) > population_size)
    {
        if (mutation_breeding_quantity != 0)
        {
            mutation_breeding_quantity -= 1;
        }
        else
        {
            replication_breeding_quantity -= 1;
        }
    }

    ASSERT((replication_breeding_quantity + crossover_breeding_quantity + mutation_breeding_quantity) == population_size);

    //
    // Clear any stage-2 preruns
    //
    _variable_bias.clear();

    LoadData();

    //
    // If source_data is set then regenete our data set
    //
    if (source_data.observations.size() != 0)
    {
        this->GenerateTrainingAndTestSet();
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates the training and test set for the environment                     
///                                                                             
///-----------------------------------------------------------------------------
void Environment::GenerateTrainingAndTestSet()
{
    std::vector<int> training;
    std::vector<int> validation;

    for(int n = 0; n < source_data.observations.size(); n++)
    {
        training.push_back(n);
    }

    int amount = static_cast<int>(source_data.observations.size()) - static_cast<int>(this->training_fraction * source_data.observations.size());

    for(int n = 0; n < amount; n++)
    {
        int m = this->_randomiser->NextInteger(static_cast<int>(training.size()));

        validation.push_back(training[m]);
        training.erase(training.begin() + m);
    }

    this->data.LoadSet(source_data, training);
    this->validation_data.LoadSet(source_data, validation);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Sets the variable-bisases required for the two-stage process's second stage 
///                                                                             
/// This is a bit of a hack since it involves changing the environment (which   
/// really should be constant) mid-run.                                         
///                                                                             
///-----------------------------------------------------------------------------
void Environment::InitialiseStage2(const std::map<int, int>& common_variables)
{
    ASSERT(_variable_bias.size() == 0);

    for(auto& var : common_variables)
    {
        _variable_bias.push_back(var.first);
        _out->Output(FileId::Stage2Inputs, "%d", var.first);
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Writes environment information to a string                                  
/// Has the secondary effect of making sure that the setup isn't garbage        
///                                                                             
///-----------------------------------------------------------------------------
void Environment::LogToFile(bool training) const
{
    if (training)
    {
        // Training
        ASSERT(population_size > 0);
        ASSERT(tree_depth > 0);
        ASSERT(common_variable_listing > 0);
        ASSERT(constant_minimum < constant_maximum);

        ASSERT(replication_breeding >= 0);
        ASSERT(crossover_breeding >= 0);
        ASSERT(mutation_breeding >= 0);
        ASSERT(discard_weakest >= 0);
        ASSERT(mutation_aftereffect >= 0);

        ASSERT(number_of_populations >= 0);
        ASSERT(migration_interval >= 0);
        ASSERT(migration_quantity >= 0);

        ASSERT(stop_after_generations >= 0);
        ASSERT(stop_after_matches >= 0);
        ASSERT(two_stage_matches >= 0);
        ASSERT(two_stage_variable_range >= 0);

        ASSERT(operators.Count() != 0);
        ASSERT(_randomiser);

        // Check specified values are valid
        ASSERT((GetCrossoverBreeding() + GetMutationBreeding() + GetReplicationBreeding()) == population_size);
    }
    else
    {
        // Evaluating
        ASSERT(program_file.size() != 0);
        ASSERT(_program != nullptr);
    }

    // Always
    ASSERT(output_directory.size());
    
    // Check the files have loaded
    ASSERT(_out);
    ASSERT(_out->directory.size());
    // ASSERT( (data.observations.size() != 0) != (source_data.observations.size() != 0) ); TODO: Fix this it should still happen!
    // ASSERT( (source_data.observations.size() == 0) || (validation_data.observations.size() == 0) ); TODO: Fix this it should still happen!
    ASSERT( (source_data.observations.size() == 0) || (training_fraction > 0) );
    ASSERT(data.variable_count() || source_data.variable_count());
    ASSERT(validation_data.observations.size() == 0 || data.variable_count() == validation_data.variable_count());

    // ASSERT(input_validation_file.size()); // don't need validation data
    // ASSERT(_validation_data.size()); // don't need validation data

    // Display everything
    _out->Output(FileId::Environment, "Mode,                                  %s", Utilities::EnumToString<ProcessingMode>(mode));
    _out->Output(FileId::Environment, "Output directory,                      %s", _out->directory.c_str());          
    _out->Output(FileId::Environment, "Input file (source data),              %s", source_data.title.c_str());         
    _out->Output(FileId::Environment, "Input file (data),                     %s", data.title.c_str());         
    _out->Output(FileId::Environment, "Input file (validation data),          %s", validation_data.title.c_str());   
    _out->Output(FileId::Environment, "");                                    
    _out->Output(FileId::Environment, "Number of source observations,         %d", source_data.observations.size());                     
    _out->Output(FileId::Environment, "Fraction of training,                  %d", training_fraction);                     
    _out->Output(FileId::Environment, "Number of observations,                %d", data.observations.size());                     
    _out->Output(FileId::Environment, "Number of validation observations,     %d", validation_data.observations.size());          
    _out->Output(FileId::Environment, "Variables per observation,             %d", data.variable_count());             
    _out->Output(FileId::Environment, "");
    if (training)
    {
        std::stringstream random_number_list;
        for(int n = 0; n < 10; n++)
        {
            random_number_list << _randomiser->NextInteger(100) << " ";
        }

        _out->Output(FileId::Environment, "Population size,                       %d", population_size);
        _out->Output(FileId::Environment, "Fitness function,                      %s", Utilities::EnumToString<FitnessFunction>(fitness_function));
        _out->Output(FileId::Environment, "Selection method,                      %s", Utilities::EnumToString<SelectionMethod>(selection_method));
        _out->Output(FileId::Environment, "Common variable listing                %f", common_variable_listing);
        _out->Output(FileId::Environment, "Constant minimum,                      %f", constant_minimum);
        _out->Output(FileId::Environment, "Constant maximum,                      %f", constant_maximum);

        _out->Output(FileId::Environment, "Breeding - replication,                %f,    ( %d individuals )", replication_breeding, GetReplicationBreeding());
        _out->Output(FileId::Environment, "Breeding - crossover,                  %f,    ( %d individuals )", crossover_breeding, GetCrossoverBreeding());              
        _out->Output(FileId::Environment, "Breeding - mutation,                   %f,    ( %d individuals )", mutation_breeding, GetMutationBreeding());                 
        _out->Output(FileId::Environment, "Discard weakest quantity,              %f,    ( %d individuals )", discard_weakest, GetDiscardWeakest());                 
        _out->Output(FileId::Environment, "Mutation,                              %f", mutation_aftereffect);
        _out->Output(FileId::Environment, "");                                    
        _out->Output(FileId::Environment, "Number of demetic populations,         %d", number_of_populations);           
        _out->Output(FileId::Environment, "Population migration interval,         %d", migration_interval);   
        _out->Output(FileId::Environment, "Population crossover quantity,         %f,    ( %d individuals )", migration_quantity, GetMigrationQuantity());   
        _out->Output(FileId::Environment, "");                                    
        _out->Output(FileId::Environment, "Maximum tree depth,                    %d", tree_depth);                      
        _out->Output(FileId::Environment, "Number of biased variables,            %d", variable_bias().size());
        _out->Output(FileId::Environment, "");
        _out->Output(FileId::Environment, "Stopping condition (generations),      %d", stop_after_generations);          
        _out->Output(FileId::Environment, "Stopping condition (fitness specific), %f", stop_after_matches);             
        _out->Output(FileId::Environment, "Stopping condition (unchanging),       %d", stop_after_unchanging);             
        _out->Output(FileId::Environment, "");
        _out->Output(FileId::Environment, "Two-stage switch (fitness specific),   %f", two_stage_matches);             
        _out->Output(FileId::Environment, "Two-stage variable range,              %d", two_stage_variable_range);             
        _out->Output(FileId::Environment, "");
        _out->Output(FileId::Environment, "Number of functional operators,        %d", operators.Count());                
        _out->Output(FileId::Environment, "Functional operators,                  %s", operators.ToString().c_str());
        _out->Output(FileId::Environment, "");                                                                           
        _out->Output(FileId::Environment, "Randomiser engine,                     %s", _randomiser->Name());
        _out->Output(FileId::Environment, "Some random numbers,                   %s", random_number_list.str().c_str());
        _out->Output(FileId::Environment, "");
        _out->Output(FileId::Environment, "Start time in seconds,                 %d", start_time);
        _out->Output(FileId::Environment, "");
        _out->Output(FileId::Environment, "Iterations in set,                     %d", iterations);
        _out->Output(FileId::Environment, "Starting iteration of set,             %d", iteration);
        _out->Output(FileId::Environment, "Current iteration,                     %d", _current_iteration);
    }
    else
    {
        _out->Output(FileId::Environment, "Fitness function,                      %s", Utilities::EnumToString<FitnessFunction>(fitness_function));
        _out->Output(FileId::Environment, "");
        std::map<int, int> common_variables;
        _program->GetCommonVariables(common_variables);
        _out->Output(FileId::Environment, "Program file,                          %s", program_file.c_str());
        _out->Output(FileId::Environment, "Program height,                        %d", _program->GetHeight());
        _out->Output(FileId::Environment, "Program variables,                     %d", common_variables.size());
    }
    _out->Output(FileId::Environment, "");
    _out->Output(FileId::Environment, "Description,                           %s", description.c_str());
}

///-----------------------------------------------------------------------------
///                                                                             
/// Time elapsed since start of simulation                                      
///                                                                             
///-----------------------------------------------------------------------------
double Environment::ElapsedTime() const
{
    return ((double)(clock() - start_time) / CLOCKS_PER_SEC);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Writes to the file-writer.                                                  
///                                                                             
///-----------------------------------------------------------------------------
void Environment::Output(FileId::Type type, const char* format, ...) const
{
    va_list args;
    va_start(args, format);
    _out->OutputVa(type, format, args);
    va_end (args);
}

void Environment::LoadData()
{
    //
    // The following variables are always fixed
    //
    if (_randomiser == nullptr)
    {
        _randomiser = new Randomiser();
    }

    //
    // Check if files have changed and need reloading
    //

    delete _out;
    std::stringstream full_dir;
    full_dir << output_directory.c_str() << "\\" << _current_iteration;
    _out = new FileWriter(full_dir.str().c_str());

    if (HasChanged(program_file, _last_program_file))
    {
        if (program_file.length() == 0)
        {
            delete _program;
            _program = nullptr;
        }
        else
        {
            std::string program_text = Utilities::ReadFileAsText(program_file.c_str());
            int index = 0;
            _program = new Program(*this, nullptr, program_text, index, 0);
        }
    }

    //
    // Start the clock
    //
    start_time = std::clock();
}

///-----------------------------------------------------------------------------
///                                                                             
/// Main processing entry point.                                                
///                                                                             
///-----------------------------------------------------------------------------
void Environment::Process()
{    
    switch(this->mode)
    {
        case ProcessingMode::train:
            Train();
            break;

        case ProcessingMode::evaluate:
            Evaluate();
            break;

        default:
            ASSERT2(false, "Unhandled switch: this->mode");
            break;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Runs the training simulation.                                               
///                                                                             
///-----------------------------------------------------------------------------
void Environment::Train()
{
    _current_iteration = iteration;

    for(int i = 0; i < iterations; i++)
    {
        std::cout << ConsoleColour::Iterations;
        std::cout << "----- Iteration " << _current_iteration << " -----";
        std::cout << ConsoleColour::Reset << std::endl;

        TrainIteration();

        // Repeat fails
        _current_iteration++;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Runs the training simulation.                                               
///                                                                             
///-----------------------------------------------------------------------------
void Environment::TrainIteration()
{
    // Initialise environment
    InitialiseStage1();

    // Display environment to the user
    LogToFile(true);

    // Record some information on the two-stage process
    int two_stage_variable_count = -1;
    int two_stage_generation = -1;
    double best_fitness = DBL_MAX;
    ProcessingStage::Type stage = ProcessingStage::normal;

    // Process the world
    World world(*this);

    for(;;)
    {
        //
        // Process this iteration
        //
        DPF("World::ProcessToEnd: ********** Iteration **********");
        ProcessResult result = world.Process();

        //
        // Log some output
        //
        printf("Time = %.1f, Gen = %d, Seen = %s, Unseen = %s, Stage = %d",
                            ElapsedTime(),
                            result.generation,
                            result.DisplayFitness(*this, true).c_str(),
                            result.DisplayFitness(*this, false).c_str(),
                            stage);

        result.LogToFile(*this);

        ASSERT(result.training_data_fitness <= best_fitness);
        best_fitness = std::max(best_fitness, result.training_data_fitness);

        //
        // Handle the two-stage case
        //
        if (result.stopreason == StopReason::TwoStage1)
        {
            // Two-stage part 1 completed
            two_stage_generation = result.generation;
            two_stage_variable_count = (int)result.common_variables.size();
            InitialiseStage2( result.common_variables );
            world.ResetPopulations();
            best_fitness = DBL_MAX;
            stage = ProcessingStage::second;
            std::cout << ConsoleColour::StoppingCondition << " (SWITCH)" << ConsoleColour::Reset;
        }

        //
        // Handle the end of the program
        //
        else if (result.stopreason != StopReason::None)
        {
            // Program ends
            printf("          \n");

            std::cout << ConsoleColour::StoppingCondition;
            std::cout << "( STOP: ";

            Output(FileId::Summary, "---------- OUTPUTS ----------");
            switch(result.stopreason)
            {
            case StopReason::Matched:
                Output(FileId::Summary, "Stopping condition   , Matched, %f", this->stop_after_matches);
                std::cout << "MATCHED " << this->stop_after_matches << " )" << std::endl;
                break;
            case StopReason::Generation:
                Output(FileId::Summary, "Stopping condition   , Generation, %d", this->stop_after_generations);
                std::cout << "GENERATION " << this->stop_after_generations << " )" << std::endl;
                break;
            case StopReason::Unchanging:
                Output(FileId::Summary, "Stopping condition   , Unchanging, %d", this->stop_after_unchanging);
                std::cout << "UNCHANGING " << this->stop_after_unchanging << " )" << std::endl;
                break;
            } 
            Output(FileId::Summary, "Generations          , %d", result.generation);
            Output(FileId::Summary, "Fitness seen         , %s", result.DisplayFitness(*this, true).c_str());
            Output(FileId::Summary, "Fitness unseen       , %s", result.DisplayFitness(*this, false).c_str());
            Output(FileId::Summary, "Common variables     , %d", result.common_variables.size());
            Output(FileId::Summary, "");
            Output(FileId::Summary, "---------- TWO-STAGE INPUTS ----------");
            Output(FileId::Summary, "Variables used       , %d", two_stage_variable_count);
            Output(FileId::Summary, "Generation activated , %d", two_stage_generation);

            std::cout << ConsoleColour::Iterations;
            std::cout << "Best program:" << std::endl;
            std::cout << ConsoleColour::Reset;
            result.best_program->ToString(std::cout, 0);
            world.WriteAllPrograms();

            
            return;
        }

        if ((result.generation % 100) == 0 || result.stopreason)
        {
            printf("          \n");
        }
        else
        {
            printf("          \r");
        }
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Evaluates an program generated by a previous training run and saved to      
/// disk                                                                        
///                                                                             
///-----------------------------------------------------------------------------
void Environment::Evaluate()
{
    LoadData();
    LogToFile(false);

    double fitness = _program->EvaluateAll(*this, data);
    double fitness2 = _program->EvaluateAll(*this, validation_data);

    std::map<int, int> common_variables;
    _program->GetCommonVariables(common_variables);

    ProcessResult result(0, fitness, fitness2, _program, common_variables, StopReason::Matched);
    result.LogToFile(*this);

    Output(FileId::Summary, "Data evaluated from existing program.");
}

