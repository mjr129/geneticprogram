#pragma once

#include "DataSet.h"
#include "FileWriter.h"
#include "Enums.h"
#include "OperatorBag.h"
#include <string>
#include <vector>
#include <ctime>
#include <ostream>
#include <map>

class Program;
class Randomiser;
class FunctionOperator;
class FileWriter;

///
/// Settings used for the simulation
///
class Environment
{
public:
    //
    // Miscellaneous
    //
    std::string description;                        /// An arbitrary comment put into the output files.
    ProcessingMode::Type mode;                      /// What feature of this application is used.
    int iteration;                                  /// This is the index of the test and the name given to the output folder.
    int iterations;                                 /// Multiple training runs may be performed by setting this value. Each run automatically increments the iteration variable.
    double common_variable_listing;                 /// What fraction of the top performing programs to list the common variables for
    double constant_minimum;                        /// Minimum value of constants generated
    double constant_maximum;                        /// Maximum value of constants generated
    
    //
    // Population
    //
    int population_size;                            /// Number of programs in the population
    int tree_depth;                                 /// Maximum depth the program trees are allowed to grow to
    FitnessFunction::Type fitness_function;         /// How fitness for each program is calculated. See the enum for details.
    SelectionMethod::Type selection_method;         /// How programs are selected for reproduction. See the enum for details.
    OperatorBag operators;                          /// Operators available to the programs in the population
    
    //
    // Replication
    //
    double replication_breeding;                    /// The fraction of individuals to generate each generation by REPLICATION
    double crossover_breeding;                      /// The fraction of individuals to generate each generation by CROSSOVER
    double mutation_breeding;                       /// The fraction of individuals to generate each generation by MUTATION
    double discard_weakest;                         /// The number of worst-fitting programs in each generation to always discard
    double mutation_aftereffect;                    /// The chance of individual programs being mutated AFTER insertion into new population

    //
    // Islands
    //
    int number_of_populations;                      /// How many populations are run in parallel
    int migration_interval;                         /// How often (in generations) migration occurs from one population to adjacent populations
    double migration_quantity;                      /// The number of programs to copy to adjacent populations when migration occurs

    //
    // Stopping conditions
    //
    int stop_after_generations;                     /// Training will stop after this many generations.
    double stop_after_matches;                      /// Training will stop when fitness reaches this value.
                                                    /// For classifier fitness function this is when the fraction of results CORRECT goes above this value.
                                                    /// For difference fitness this is when the average distance to the correct result goes below this value.
    int stop_after_unchanging;                      /// Training will stop if fitness sees no improvement over this many generations.

    //
    // Two-stage
    //
    double two_stage_matches;                       /// This has the same semantics as stop_after_matches.
                                                    /// When this stopping condition is reached training will RESTART using the previously selected variables.
    int two_stage_variable_range;                   /// This is how many neighbours of the two stage variables to choose within (1 means 1 to either side, i.e. a total range of 3)

    //
    // I/O
    //
    DataSet source_data;                            /// As an alternative to "data" and "validation_data" this can be set to generate the below two automatically.
    double training_fraction;                       /// If source_data is set we want to know how much is the training set.
    DataSet data;                                   /// Data sets
    DataSet validation_data;                        /// Data sets, used in validation only    std::string output_directory;                   /// < Where the output files are placed
    std::string output_directory;                   /// < Where the output files are placed

    //
    // Playback (classification) controls
    //
    std::string program_file;                       /// < GP Program to run. If this is EMPTY training will be performed and a new program will be determined rather than using an existing one.

    //
    // Private
    //
private:
    std::string _last_input_data_file;              /// < Used to monitor change-of-stage
    std::string _last_input_validation_file;        /// < Used to monitor change-of-stage
    std::string _last_output_directory;             /// < Used to monitor change-of-stage
    std::string _last_program_file;
    int _current_iteration;                         /// Current iteration of the training procedure.

    FileWriter* _out;
    Randomiser* _randomiser;                        /// < Random number generator
    clock_t start_time;                             /// < Start time

    int replication_breeding_quantity;              /// < Number of individuals to generate by REPLICATION
    int crossover_breeding_quantity;                /// < Number of individuals to generate by CROSSOVER
    int mutation_breeding_quantity;                 /// < Number of individuals to generate by MUTATION

    std::vector<int> _variable_bias;                /// < Bias picking specific variables

    Program* _program;                              /// < Program to run and test data against.

    //
    // Methods
    //
public:
    Environment() : _out(nullptr), _randomiser(nullptr) { }
    ~Environment();
    DISALLOW_COPY(Environment);

    double ElapsedTime() const;
    void Output(FileId::Type type, const char* format, ...) const;

    int GetReplicationBreeding() const { return replication_breeding_quantity;   }
    int GetDiscardWeakest() const      { return (int)(discard_weakest       * population_size + 0.5);   }
    int GetCrossoverBreeding() const   { return crossover_breeding_quantity;   }
    int GetMutationBreeding() const    { return mutation_breeding_quantity;   }
    int GetMigrationQuantity() const   { return (int)(migration_quantity    * population_size + 0.5);   }

    Randomiser& randomiser() const                              { return *_randomiser; }
    const std::vector<int> variable_bias() const                { return _variable_bias; }

    void Process();

private:
    void Train();
    void TrainIteration();
    void Evaluate();
    void LoadData();
    void InitialiseStage1();
    void InitialiseStage2(const std::map<int, int>& common_variables);
    void LogToFile(bool training) const;
    void GenerateTrainingAndTestSet();
};

