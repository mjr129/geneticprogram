#include "Common.h"
#include "EvaluationResult.h"
#include "Program.h"

///----------------------------------------------------------------------------
///                                                                            
/// Constructor                                                                
///                                                                            
///----------------------------------------------------------------------------
ProgramResult::ProgramResult(Program* program, double range) 
    : program(program)
    , range(range) 
{
}

/*
///----------------------------------------------------------------------------
///                                                                            
/// Constructor                                                                
///                                                                            
///----------------------------------------------------------------------------
PopulationResult::PopulationResult() 
    : best_program(nullptr)
    , best_matches(0)
{ 
}

///----------------------------------------------------------------------------
///                                                                            
/// Constructor                                                                
///                                                                            
///----------------------------------------------------------------------------
PopulationResult::PopulationResult(Program* best_program, int best_matches) 
    : best_program(best_program)
    , best_matches(best_matches)
{ 
}

///----------------------------------------------------------------------------
///                                                                            
/// Merges two sets of results                                                 
///                                                                            
///----------------------------------------------------------------------------
void PopulationResult::Append(const PopulationResult& copy_from)
{
    if (copy_from.best_matches > best_matches || best_program == nullptr)
    {
        best_matches = copy_from.best_matches;
        best_program = copy_from.best_program;
    }
}         */

