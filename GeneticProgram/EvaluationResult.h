#pragma once

#include <vector>

class Program;

class ProgramResult
{
public:
    /// Program
    Program* program;

    /// This is the weighting used to select this individual.
    /// It is the weight of this individual, plus all individuals that appear before it in the list.
    double range;

    /// Constructor
    ProgramResult(Program* program, double range);
};

/*class PopulationResult
{
public: 
    /// Best performing program
    Program* best_program;

    /// Constructor
    PopulationResult();

    /// Constructor
    PopulationResult(Program* best_program, int best_matches);

    /// Merges two sets of results
    void Append(const PopulationResult& copy_from);
};
*/