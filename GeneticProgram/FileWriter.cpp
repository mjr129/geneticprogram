#include "Common.h"
#include "FileWriter.h"

#include <cstdarg>
#include <sstream>
#include <iostream>
#include <filesystem>

///----------------------------------------------------------------------------
///                                                                            
/// Constructor                                                                
///                                                                            
/// Creates output directory and file-streams ready for writing to.            
///                                                                            
///----------------------------------------------------------------------------
FileWriter::FileWriter(const char* directory)
    : directory(directory)
{
    // std::cout << "Writing to " << directory << std::endl;
    std::tr2::sys::create_directories(std::tr2::sys::path(directory));

    std::stringstream fn_Generations;
    fn_Generations << directory << "\\generations.csv";
    std::stringstream fn_CommonVariables;
    fn_CommonVariables << directory << "\\common_variables.csv";
    std::stringstream fn_Environment;
    fn_Environment << directory << "\\environment.csv";
    std::stringstream fn_BestProgram;
    fn_BestProgram << directory << "\\best_program.csv";
    std::stringstream fn_Summary;
    fn_Summary << directory << "\\_summary.csv";
    std::stringstream fn_Predictions;
    fn_Predictions << directory << "\\predictions.csv";
    std::stringstream fn_AllPrograms;
    fn_AllPrograms << directory << "\\all_programs.csv";
    std::stringstream fn_Stage2Inputs;
    fn_Stage2Inputs << directory << "\\stage_2_inputs.csv";

    files[FileId::Generations]      = fopen(fn_Generations.str().c_str(), "w");
    files[FileId::CommonVariables]  = fopen(fn_CommonVariables.str().c_str(), "w");
    files[FileId::Environment]      = fopen(fn_Environment.str().c_str(), "w");
    files[FileId::BestProgram]      = fopen(fn_BestProgram.str().c_str(), "w");
    files[FileId::Summary]          = fopen(fn_Summary.str().c_str(), "w");
    files[FileId::Predictions]      = fopen(fn_Predictions.str().c_str(), "w");
    files[FileId::AllPrograms]      = fopen(fn_AllPrograms.str().c_str(), "w");
    files[FileId::Stage2Inputs]     = fopen(fn_Stage2Inputs.str().c_str(), "w");

    ASSERT2(files[FileId::Generations], fn_Generations.str().c_str());
    ASSERT2(files[FileId::CommonVariables], fn_CommonVariables.str().c_str());
    ASSERT2(files[FileId::Environment], fn_Environment.str().c_str());
    ASSERT2(files[FileId::BestProgram], fn_BestProgram.str().c_str());
    ASSERT2(files[FileId::Summary], fn_Summary.str().c_str());
    ASSERT2(files[FileId::Predictions], fn_Predictions.str().c_str());
    ASSERT2(files[FileId::AllPrograms], fn_AllPrograms.str().c_str());
    ASSERT2(files[FileId::Stage2Inputs], fn_Stage2Inputs.str().c_str());

    Output(FileId::Generations, "time, generation, seen, unseen");
    Output(FileId::CommonVariables, "variable, count");
    Output(FileId::Environment, "name, value");
    Output(FileId::Predictions, "observation, known, predicted");
    Output(FileId::AllPrograms, "island, index, raw_fitness, program");
    Output(FileId::Stage2Inputs, "index");
}

///----------------------------------------------------------------------------
///                                                                            
/// Destructor                                                                 
///                                                                            
///----------------------------------------------------------------------------
FileWriter::~FileWriter()
{
    for(auto& v : files)
    {
        fclose(v.second);
    }

    files.clear();
}

///----------------------------------------------------------------------------
///                                                                            
/// Writes data to the specified output stream.                                
///                                                                            
///----------------------------------------------------------------------------
void FileWriter::Output(FileId::Type type, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    OutputVa(type, format, args);
    va_end (args);
}

///----------------------------------------------------------------------------
///                                                                            
/// Writes data to the specified output stream.                                
/// Uses va_list rather than ...                                               
///                                                                            
///----------------------------------------------------------------------------
void FileWriter::OutputVa(FileId::Type type, const char* format, va_list args)
{
    FILE* file = files[type];
    vfprintf(file, format, args);
    fprintf(file, "\n");
    fflush(file);
}
