#pragma once

#include <map>
#include <cstdio>
#include <string>

namespace FileId
{
    enum Type
    {
        Generations,
        CommonVariables,
        Environment,
        BestProgram,
        Summary,
        Predictions,
        AllPrograms,
        Stage2Inputs,
    };
};

class FileWriter
{
private:
    std::map<FileId::Type, FILE*> files;
public:
    std::string directory;

public:
    FileWriter(const char* directory);
    ~FileWriter();
    void Output(FileId::Type type, const char* format, ...);
    void OutputVa(FileId::Type type, const char* format, va_list args);
};