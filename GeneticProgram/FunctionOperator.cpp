#include "FunctionOperator.h"

///----------------------------------------------------------------------------
///                                                                            
/// Constructor.                                                               
///                                                                            
///----------------------------------------------------------------------------
FunctionOperator::FunctionOperator(const char* name, DataFunction function, int inputs)
    : name(name)
    , number_of_inputs(inputs)
    , function_pointer(function)
{
    // No further action
}