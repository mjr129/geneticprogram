#pragma once

class Program;

typedef double (*DataFunction)(double* inputs);

class FunctionOperator
{
public:
    const char* name;
    int number_of_inputs;
    DataFunction function_pointer;

public:
    FunctionOperator(const char* name, DataFunction function, int inputs);
};
