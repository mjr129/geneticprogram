#include "Common.h"
#include "OperatorBag.h"
#include "FunctionOperator.h"
#include "Randomiser.h"
#include "Utilities.h"
#include <sstream>

// Checks such as "if (value == 0) then ...dont...divide..."
// and are only done for similarity with davis's program
// In essence such checks are either pointless or only touch the tip of the iceberg.
// Bad values need to be (and are) handled by the calling code - "x / 0 = error" might
// be easy to catch here, but "small_value * small_value = out_of_range" needs some more
// thorough checking.

std::vector<FunctionOperator> OperatorBag::all_operators;

/// PI
static const double PI = 3.14159265359;

/// Value returned when, for instance, dividing by zero
static const double HIGHVAL = 1.0;

/// a + b
static double fn_add_2(double* inputs) 
{				
    return inputs[0] + inputs[1];	   
}

/// a - b
static double fn_sub_2(double* inputs) 
{				
    return inputs[0] - inputs[1];	   
}

/// a * b
static double fn_mul_2(double* inputs) 
{				
    return inputs[0] * inputs[1];	   
}

/// a / b
static double fn_div_2(double* inputs) 
{									   
    if (inputs[1] == 0.0)
    {
        return HIGHVAL;
    }

    return inputs[0] / inputs[1];	   
}		

/// a > b
static double fn_gt_2(double* inputs)
{
    return (inputs[0] > inputs[1]) ? 1.0 : 0.0;
}

/// a < b
static double fn_lt_2(double* inputs)
{
    return (inputs[0] < inputs[1]) ? 1.0 : 0.0;
}

/// max(a, b)
static double fn_max_2(double* inputs)
{
    return std::max(inputs[0], inputs[1]);
}

/// min(a, b)
static double fn_min_2(double* inputs)
{
    return std::min(inputs[0], inputs[1]);
}

/// a == b
static double fn_eq_2(double* inputs)
{
    return (inputs[0] == inputs[1]) ? 1.0 : 0.0;
}

/// a != b
static double fn_neq_2(double* inputs)
{
    return (inputs[0] != inputs[1]) ? 1.0 : 0.0;
}

/// (a + b) / 2
static double fn_avg_2(double* inputs)
{
    return (inputs[0] + inputs[1]) / 2.0;
}

/// |a|
static double fn_abs_1(double* inputs)
{
    return (inputs[0] > 0) ? inputs[0] : (-inputs[0]);
}

/// sin(a)
static double fn_sin_1(double* inputs)
{
    return std::sin(inputs[0] * PI);
}

/// cos(a)
static double fn_cos_1(double* inputs)
{
    return std::cos(inputs[0] * PI);
}

/// tan(a)
static double fn_tan_1(double* inputs)
{
    return std::tan(inputs[0] * PI);
}

/// sqrt(a)
static double fn_sqrt_1(double* inputs)
{
    if (inputs[0] == 0.0)
    {
        return 0.0;
    }

    return std::sqrt(std::abs(inputs[0]));
}

/// log(a)
static double fn_log_1(double* inputs)
{
    if (inputs[0] == 0.0)
    {
        return HIGHVAL;
    }

    return std::log(std::abs(inputs[0]));
}

/// a ^ 2
static double fn_sqr_1(double* inputs)
{
    return inputs[0] * inputs[0];
}

/// 1 / a
static double fn_inv_1(double* inputs)
{
    if (inputs[0] == 0.0)
    {
        return HIGHVAL;
    }

    return 1.0 / inputs[0];
}

///----------------------------------------------------------------------------
///                                                                            
/// Populates target with all available operators                              
///                                                                            
///----------------------------------------------------------------------------
void OperatorBag::PopulateFromList(const std::string& list)
{
    operators.clear();

    std::string element;
    size_t cursor = 0;
    auto& all = AllOperators();
    
    while(Utilities::Tokenise(list, " \t,", cursor, element))
    {
        for(const FunctionOperator& op : all)
        {
            if (op.name == element || element == "ALL")
            {
                operators.push_back(&op);
            }
        }
    }
}

const std::vector<FunctionOperator>& OperatorBag::AllOperators()
{
    if (all_operators.size() == 0)
    {
          all_operators.push_back( FunctionOperator( "ADD", &fn_add_2, 2));
          all_operators.push_back( FunctionOperator( "SUB", &fn_sub_2, 2));
          all_operators.push_back( FunctionOperator( "MUL", &fn_mul_2, 2));
          all_operators.push_back( FunctionOperator( "DIV", &fn_div_2, 2));
          all_operators.push_back( FunctionOperator( "GT",  &fn_gt_2,  2));
          all_operators.push_back( FunctionOperator( "LT",  &fn_lt_2,  2));
          all_operators.push_back( FunctionOperator( "MAX", &fn_max_2, 2));
          all_operators.push_back( FunctionOperator( "MIN", &fn_min_2, 2));
          all_operators.push_back( FunctionOperator( "EQ",  &fn_eq_2,  2));
          all_operators.push_back( FunctionOperator( "NEQ", &fn_neq_2, 2));
          all_operators.push_back( FunctionOperator( "AVG", &fn_avg_2, 2));
          all_operators.push_back( FunctionOperator( "ABS", &fn_abs_1, 1));
          all_operators.push_back( FunctionOperator( "SIN", &fn_sin_1, 1));
          all_operators.push_back( FunctionOperator( "COS", &fn_cos_1, 1));
          all_operators.push_back( FunctionOperator( "TAN", &fn_tan_1, 1));
          all_operators.push_back( FunctionOperator( "SQRT",&fn_sqrt_1,1));
          all_operators.push_back( FunctionOperator( "LOG", &fn_log_1, 1));
          all_operators.push_back( FunctionOperator( "SQR", &fn_sqr_1, 1));
          all_operators.push_back( FunctionOperator( "INV", &fn_inv_1, 1));
    }

    return all_operators;
}

const FunctionOperator& OperatorBag::SelectAtRandom(Randomiser& randomiser) const
{
    return *operators[randomiser.NextInteger(static_cast<int>(operators.size()))];
}

const FunctionOperator& OperatorBag::SelectByName(const std::string& name)
{
    auto& all = AllOperators();

    for(const FunctionOperator& op : all)
    {
        if (op.name == name)
        {
            return op;
        }
    }

    ASSERT2(false, (std::string("Operator not found: ") + name).c_str());
    return all_operators[0];
}

std::string OperatorBag::ToString() const
{
    std::stringstream result;

    for(auto it = operators.begin(); it != operators.end(); ++it)
    {
        result << (*it)->name << " ";
    }

    return result.str();
}

int OperatorBag::Count() const
{
    return static_cast<int>(operators.size());
}