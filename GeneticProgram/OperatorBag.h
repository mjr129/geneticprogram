#pragma once

class FunctionOperator;
class Randomiser;

#include <vector>

///----------------------------------------------------------------------------
///                                                                            
/// Populates a list with the function operators used by the Programs (add,    
/// subtract, divide, multiply) etc.                                           
///                                                                            
///----------------------------------------------------------------------------
class OperatorBag
{
private:
    static std::vector<FunctionOperator> all_operators;
    std::vector<const FunctionOperator*> operators;

public:
    void PopulateFromList(const std::string& list);
    const FunctionOperator& SelectAtRandom(Randomiser& randomiser) const;
    static const FunctionOperator& SelectByName(const std::string& name);
    std::string ToString() const;
    int Count() const;

private:
    static const std::vector<FunctionOperator>& AllOperators();
};
