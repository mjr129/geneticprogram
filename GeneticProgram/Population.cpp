#include "Common.h"
#include "Population.h"

#include "Environment.h"
#include "Program.h"
#include "Randomiser.h"
#include "EvaluationResult.h"

#include <algorithm>
#include <iostream>
#include <thread>
#include <future>


///-----------------------------------------------------------------------------
///                                                                             
/// Initialises a random population                                             
///                                                                             
///-----------------------------------------------------------------------------
Population::Population(const Environment& environment)
    : _environment(environment)
{
    // Now randomise the initial population
    while(_programs.size() < (uint)environment.population_size)
    {
        _programs.push_back(new Program(environment, nullptr, 0, 0));
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Destructor                                                                  
///                                                                             
///-----------------------------------------------------------------------------
Population::~Population()
{
    // Delete the population
    for(auto it = _programs.begin(); it != _programs.end(); ++it)
    {
        delete *it;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Used for sorting results.                                                   
///                                                                             
///-----------------------------------------------------------------------------
bool Population::SortFunction(const Program* a, const Program* b)
{
    // Lower raw fitness is better
    return a->GetFitness() < b->GetFitness();
}

///-----------------------------------------------------------------------------
///                                                                             
/// Determine fitness of all programs in the population                         
/// and sorts the programs based on fitness.                                    
///                                                                             
///-----------------------------------------------------------------------------
Program* Population::Evaluate()
{
    for(uint index = 0; index < _programs.size(); index++)
    {
        Program& program = *(_programs[index]);

        if (!program.HasFitness())
        {
            double fitness = program.EvaluateAll(_environment, _environment.data);
            program.SetFitness(fitness);
        }
    }

    DPF("Evaluate: Evaluated programs (size %d)", _programs.size());

    SortPrograms();

    return _programs[0];
}

///-----------------------------------------------------------------------------
///                                                                             
/// Sorts the programs of this population based on fitness.                     
///                                                                             
///-----------------------------------------------------------------------------
void Population::SortPrograms()
{
    std::sort(_programs.begin(), _programs.end(), SortFunction);
    DPF("SortPrograms: Sorted programs (size %d)", _programs.size());
}

///-----------------------------------------------------------------------------
///                                                                             
/// Processes 1 generation of the population                                    
///                                                                             
/// @return Best fitting program                                                
///                                                                             
///-----------------------------------------------------------------------------
void Population::NextGeneration()
{   
    //
    // Calculate selection weightings
    //
    std::vector<ProgramResult> results;
    double total_weight = 0.0;

    switch( _environment.selection_method )
    {
        case SelectionMethod::fitness:
            for(Program* program : _programs)
            {
                double raw_fitness = program->GetFitness();
                double weight = 1.0 / (1.0 + raw_fitness);  // adjusted fitness
                total_weight += weight;                     // total fitness

                results.push_back(ProgramResult(program, total_weight));
            }
            break;

        case SelectionMethod::rank:
            for(size_t index = 0; index < _programs.size(); index++)
            {
                Program* program = _programs[index];
                double weight = static_cast<double>(_programs.size() - index);
                total_weight += weight;

                results.push_back(ProgramResult(program, total_weight));
            }
            break;

        default:
            ASSERT2( false, "Unhandled switch: _environment.selection_method" );
            break;
    }

    ASSERT(results[results.size() - 1].range > 0);
    DPF("NextGeneration: Accumulated total selection weights %f (holding size %d)", total_weight, results.size());

    //
    // Clear the previous generation
    //
    _programs.clear();
    DPF("NextGeneration: Cleared previous generation (size %d)", _programs.size());

    //
    // BREED: Replication
    //
    // Elitism
    //
    for(int n = 0; n < _environment.GetReplicationBreeding(); n++)
    {
        _programs.push_back( Program::Clone(*results[n].program) );
    }
    DPF("NextGeneration: Preserved strongest (size %d)", _programs.size());

    //
    // DISCARD
    //
    // Reverse elitism
    // This will also discard any surplus we have from migration from an
    // adjacent population.
    //
    while(results.size() > (uint)(_environment.population_size - _environment.GetDiscardWeakest()))
    {
        /* Program* program = results[results.size() - 1].program;
        double raw_fitness = program->GetFitness();
        double adjusted_fitness = 1.0 / (1.0 + raw_fitness);
        double normalised_fitness = adjusted_fitness / results.back().range;
        DPF("-- DISCARD: Raw fitness = %f, Adj fitness = %f, Norm fitness = %f", raw_fitness, adjusted_fitness, normalised_fitness); */

        delete results[results.size() - 1].program;
        results.pop_back();
    }
    DPF("NextGeneration: Discarded weakest (holding size %d)", results.size());

    // Debug
    /* int id = 0;
    for(ProgramResult& result : results)
    {
        Program* program = result.program;
        double raw_fitness = program->GetFitness();
        double adjusted_fitness = 1.0 / (1.0 + raw_fitness);
        double normalised_fitness = adjusted_fitness / results.back().range;
        DPF("-- Prog %d: Raw fitness = %f, Adj fitness = %f, Norm fitness = %f", id++, raw_fitness, adjusted_fitness, normalised_fitness);
    } */

    //
    // BREED: Crossover
    //
    // Sexual reproduction
    //
    for(int n = 0 ; n < _environment.GetCrossoverBreeding(); n += 2)
    {
        Program* parent1 = ChooseRandomNode(results);
        Program* parent2 = ChooseRandomNode(results);
        Program* baby1;
        Program* baby2;
        Program::Breed(_environment, *parent1, *parent2, &baby1, &baby2);
        _programs.push_back( baby1 );
        _programs.push_back( baby2 );

        
#ifdef GENETIC_DEBUG
        std::cout << "GENETICS----BREED" << std::endl;
        std::cout << "    Parent 1:" << std::endl; parent1->ToString(std::cout, 4);
        std::cout << "    Parent 2:" << std::endl; parent2->ToString(std::cout, 4);
        std::cout << "    Baby 1:" << std::endl; baby1->ToString(std::cout, 4);
        std::cout << "    Baby 2:" << std::endl; baby2->ToString(std::cout, 4);
#endif
    }
    DPF("NextGeneration: Breed += %d (size %d)", _environment.GetCrossoverBreeding(), _programs.size());

    ASSERT((int)_programs.size() <= _environment.population_size);

    //
    // BREED: Mutation
    //
    // Asexual reproduction
    //
    for(int n = 0; n < _environment.GetMutationBreeding(); n++)
    {
        Program* parent = ChooseRandomNode(results);  
        Program* baby = Program::Mutate(_environment, *parent );
        _programs.push_back( baby );
#ifdef GENETIC_DEBUG
        std::cout << "GENETICS----MUTATE" << std::endl;
        std::cout << "    Parent:" << std::endl; parent->ToString(std::cout, 4);
        std::cout << "    Baby:" << std::endl; baby->ToString(std::cout, 4);
#endif
    }
    DPF("NextGeneration: Mutate (size %d)", _programs.size());

    ASSERT(static_cast<int>(_programs.size()) == _environment.population_size);

    //
    // MUTATION: Existing
    //
    for(int n = 0; n < static_cast<int>(_programs.size()); n++)
    {
        if (_environment.randomiser().NextDouble(1) < _environment.mutation_aftereffect)
        {
            Program* parent = _programs[n];
            Program* baby = Program::Mutate(_environment, *parent);

            double parent_fitness = parent->HasFitness() ? parent->GetFitness() : parent->EvaluateAll(_environment, _environment.data);
            double baby_fitness = baby->EvaluateAll(_environment, _environment.data);

            if (baby_fitness < parent_fitness)
            {
                delete parent;
                baby->SetFitness(baby_fitness);             // Don't need to reevaluate this next round
                _programs[n] = baby;
            }
            else
            {
                delete baby;
                if (!parent->HasFitness())
                {
                    parent->SetFitness(parent_fitness);     // Don't need to reevaluate this next round
                }
            }
        }
    }
    DPF("NextGeneration: Mutate existing (size %d)", _programs.size());
    
    //
    // Now discard the old population
    //
    while(results.size() != 0)
    {
        delete results[results.size() - 1].program;
        results.pop_back();
    }
    DPF("NextGeneration: Discarded holding (holding size %d)", results.size());
}

///-----------------------------------------------------------------------------
///                                                                             
/// Migrates nodes from this population into another population.                
///                                                                             
///-----------------------------------------------------------------------------
void Population::MigrateInto(Population& population)
{
    // Copy the best nodes from this population into another population
    for(int n = 0; n < _environment.GetMigrationQuantity(); n++)
    {
        population._programs.push_back( Program::Clone( *_programs[n] ) );
    }

    population.SortPrograms();

    DPF("MigrateInto: Migrated (size %d)", population._programs.size());
}

///-----------------------------------------------------------------------------
///                                                                             
/// Selects a random node from the list of results.                             
///                                                                             
///-----------------------------------------------------------------------------
Program* Population::ChooseRandomNode(std::vector<ProgramResult> results)
{
    double max = results[results.size() - 1].range;
    double value = _environment.randomiser().NextDouble(max);

    for(uint n = 0; n < results.size(); n++)
    {
        if (value < results[n].range)
        {
            return results[n].program;
        }
    }

    ASSERT(false);
    return nullptr;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Returns variables encountered in the top 10% of programs                    
///                                                                             
///-----------------------------------------------------------------------------
void Population::GetCommonVariables(double fraction, std::map<int, int>& map)
{
    int ten_percent = (int)(_programs.size() * fraction);

    for(int n = 0; n < ten_percent; n++)
    {
        _programs[n]->GetCommonVariables(map);
    }
}
