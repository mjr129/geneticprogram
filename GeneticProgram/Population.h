#pragma once

class Program;
class Randomiser;

class Environment;
class ProgramResult;
class PopulationResult;

#include <vector>
#include <map>

///----------------------------------------------------------------------------
///                                                                            
/// Manages a population of Genetic-Programs                                   
///                                                                            
///----------------------------------------------------------------------------
class Population
{
public: // TODO: Only for checking
	const Environment& _environment;
	std::vector<Program*> _programs;

public:
	Population(const Environment& environment);
    DISALLOW_COPY(Population);
    ~Population();

    Program* Evaluate();
	void NextGeneration();
    void MigrateInto(Population& population);
    void GetCommonVariables(double fraction, std::map<int, int>& map);
	
private:
    void SortPrograms();
    static bool SortFunction(const Program* a, const Program* b);
    Program* ChooseRandomNode(std::vector<ProgramResult> results);
};
