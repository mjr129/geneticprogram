#include "Common.h"
#include "ProcessResult.h"
#include "Environment.h"
#include "Program.h"
#include <sstream>

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
///-----------------------------------------------------------------------------
ProcessResult::ProcessResult()
    : stopreason(StopReason::None)
    , generation(0)
    , training_data_fitness(0)
    , validation_data_fitness(0)
    , best_program(nullptr)
    , common_variables()
{
}

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
///-----------------------------------------------------------------------------
ProcessResult::ProcessResult(int generation, double training_data_fitness, double validation_data_fitness)
    : stopreason(StopReason::None)
    , generation(generation)
    , training_data_fitness(training_data_fitness)
    , validation_data_fitness(validation_data_fitness)
    , best_program(nullptr)
    , common_variables()
{
}

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
///-----------------------------------------------------------------------------
ProcessResult::ProcessResult(int generation, double training_data_fitness, double validation_data_fitness, Program* best_program, const std::map<int, int>& common_variables, StopReason::Type stopreason)
    : stopreason(stopreason)
    , generation(generation)
    , training_data_fitness(training_data_fitness)
    , validation_data_fitness(validation_data_fitness)
    , best_program(best_program)
    , common_variables(common_variables)
{
}

///-----------------------------------------------------------------------------
///                                                                             
/// Logs this result to the various output files                                
///                                                                             
///-----------------------------------------------------------------------------
void ProcessResult::LogToFile(const Environment& environment) const
{
    environment.Output(FileId::Generations, "%.1f, %d, %s, %s",
                            environment.ElapsedTime(),
                            generation,
                            DisplayFitness(environment, true).c_str(),
                            DisplayFitness(environment, false).c_str());

    if (   stopreason != StopReason::None
        && stopreason != StopReason::TwoStage1)
    {
        std::stringstream ss;
        best_program->ToString(ss, 0);
        environment.Output(FileId::BestProgram, ss.str().c_str());

        for( auto& v : common_variables)
        {
            environment.Output(FileId::CommonVariables, "%d, %d", v.first, v.second);
        }

        for(size_t index = 0; index < environment.data.observations.size(); index++)
        {
            const Observation& observation = environment.data.observations[index];
            double prediction = best_program->Evaluate(observation.data);
            environment.Output(FileId::Predictions, "%d, %f, %f", index, observation.expected, prediction);
        }
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Gets the fitness of this result as suitable for display purposes            
///                                                                             
///-----------------------------------------------------------------------------
std::string ProcessResult::DisplayFitness(const Environment& environment, bool training) const
{
    char dest[32];

    double fitness = training ? training_data_fitness : validation_data_fitness;
    int elements = static_cast<int>(training ? environment.data.observations.size() : environment.validation_data.observations.size());

    switch(environment.fitness_function)
    {
        case FitnessFunction::classifier:
            sprintf(dest, "%.1f", (1 - (fitness / elements)) * 100.0);
            break;

        case FitnessFunction::difference:
            sprintf(dest, "%f", fitness / elements);
            break;

        default:
            ASSERT2(false, "Unhandled switch: environment.fitness_function");
            break;
    }

    return std::string(dest);
}