#pragma once

#include "Enums.h"
#include <map>

class Program;
class Environment;

///-----------------------------------------------------------------------------
///                                                                             
/// Used to hold the results of World iterations, as well as the final result.  
///                                                                             
///-----------------------------------------------------------------------------
class ProcessResult
{
public:
    StopReason::Type stopreason;            /// < Reason for stopping the simulation
    int generation;                         /// < Current generation
    double training_data_fitness;           /// < How well the best program matches the training data
    double validation_data_fitness;         /// < How well the best training-data program matches the validation data

    Program* best_program;                  /// < The best program (only set for the final result)
    std::map<int, int> common_variables;    /// < The most frequently used variables (only set for the final result)

public:
    ProcessResult();
    ProcessResult(int generation, double training_data_fitness, double validation_data_fitness);
    ProcessResult(int generation, double training_data_fitness, double validation_data_fitness, Program* best_program, const std::map<int, int>& common_variables, StopReason::Type stopreason);

    void LogToFile(const Environment& environment) const;
    std::string DisplayFitness(const Environment& environment, bool training) const;
};
