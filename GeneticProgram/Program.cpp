#include "Common.h"
#include "Program.h"

#include "Environment.h"
#include "Randomiser.h"
#include "FunctionOperator.h"
#include "DataSet.h"
#include "Utilities.h"
#include "ConsoleColour.h"

#include <iostream>

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
/// Generates a random function-node                                            
///                                                                             
///-----------------------------------------------------------------------------
Program::Program(const Environment& environment, Program* parent, int index_in_parent, int depth)
    : _parent(parent)
    , _index_in_parent(index_in_parent)
    , _depth(depth)
    , _fitness(0)
    , _has_fitness(false)
{
    for(int n = 0; n < _max_children; n++)
    {
        _children[n] = nullptr;
    }

    _number_of_children = 0;
    PickRandomOperator(environment);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
/// Creates a deep copy of a node.                                              
///                                                                             
///-----------------------------------------------------------------------------
Program::Program(const Program& copyFrom, Program* parent, int index_in_parent)
    : _parent(parent)
    , _index_in_parent(index_in_parent)
    , _function_operator(copyFrom._function_operator)
    , _input_index(copyFrom._input_index)
    , _constant_value(copyFrom._constant_value)
    , _number_of_children(copyFrom._number_of_children)
    , _depth(parent ? (parent->_depth + 1) : 0)
    , _fitness(0)
    , _has_fitness(false)
    , _type(copyFrom._type)
{
    for (int n = 0; n < _number_of_children; n++)
    {
        _children[n] = new Program(*(copyFrom._children[n]), this, n);
    }

    for (int n = _number_of_children; n < _max_children; n++)
    {
        _children[n] = nullptr;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
/// Creates a program from its textual representation.                          
///                                                                             
///-----------------------------------------------------------------------------
Program::Program(const Environment& environment, Program* parent, std::string& text, int& index, int index_in_parent)
{
    // The basics
    _parent = parent;
    _depth = (parent == nullptr) ? 0 : (parent->_depth + 1);
    _index_in_parent = index_in_parent;

    // Read past any commas
    int start_index = index;

    while(text[index] == ',' && index < static_cast<int>(text.length()))
    {
        index++;
    }

    ASSERT((index - start_index) == _depth);

    // Read the name
    start_index = index;
    while(text[index] != '\r' && text[index] != '\n' && text[index] != ':' && index < static_cast<int>(text.length()))
    {
        index++;
    }

    std::string name = text.substr(start_index, index - start_index);
    std::string value = "";

    // Read any value
    if (text[index] == ':')
    {
        index++;
        start_index = index;

        while(text[index] != '\r' && text[index] != '\n' && index < static_cast<int>(text.length()))
        {
            index++;
        }

        value = text.substr(start_index, index - start_index);
    }

    // Read to the end of the line
    while(text[index] == '\r' || text[index] == '\n' && index < static_cast<int>(text.length()))
    {
        index++;
    }

    // Now interpret the result
    if(name == "V")
    {
        _type = OperatorType::Variable;
        _input_index = atoi(value.c_str());
        _constant_value = 0.0;
        _function_operator = nullptr;
        _number_of_children = 0;

        ASSERT(_input_index > 0);
        ASSERT(_input_index < static_cast<int>(environment.data.observations[0].data.size()));
    }
    else if(name == "C")
    {
        _type = OperatorType::Constant;
        _input_index = 0;
        _constant_value = atof(value.c_str());
        _function_operator = nullptr;
        _number_of_children = 0;
    }
    else
    {
        _type = OperatorType::Function;
        _input_index = 0;
        _constant_value = 0.0;

        _function_operator = &OperatorBag::SelectByName(name);

        ASSERT2(_function_operator, name.c_str());

        for(int n = 0; n < _function_operator->number_of_inputs; n++)
        {
            _children[n] = new Program(environment, this, text, index, n);
        }

        _number_of_children = _function_operator->number_of_inputs;
    }

    _fitness = 0;
    _has_fitness = false;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Destructor                                                                  
///                                                                             
///-----------------------------------------------------------------------------
Program::~Program()
{
    // Delete the children
    for (int n = 0; n < _number_of_children; n++)
    {
        delete _children[n];
        _children[n] = nullptr;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Changes the operator of this node to a random one.                          
///                                                                             
/// Corrects the number of children by deleting them or inserting random ones   
///                                                                             
///-----------------------------------------------------------------------------
void Program::PickRandomOperator(const Environment& environment)
{
    // Pick a random operator
    bool pick_variable = 
        (_depth >= (environment.tree_depth - 1)) ||
        environment.randomiser().NextBoolean();

    int old_children = _number_of_children;

    if (pick_variable)
    {
        // No operator, just use a random input-value
        _function_operator = nullptr;
        _number_of_children = 0;
        bool pick_variable = environment.randomiser().NextBoolean();

        if (pick_variable)
        {
            _type = OperatorType::Variable;

            if (environment.variable_bias().size() != 0)
            {   
                // Two-stage-phase-2: Pick from specific variables
                int bias_index = environment.randomiser().NextInteger( static_cast<int>(environment.variable_bias().size()) );

                _input_index = environment.variable_bias()[bias_index];
                _input_index += environment.randomiser().NextInteger( environment.two_stage_variable_range * 2 + 1 ) - environment.two_stage_variable_range;

                if (_input_index < 0) _input_index = 0;
                if (_input_index >= (int)environment.data.variable_count()) _input_index = ((int)environment.data.variable_count() - 1);
            }
            else
            {
                // One-stage or two-stage-phase-1: Any variable
                _input_index = environment.randomiser().NextInteger( environment.data.variable_count() );
            }
        }
        else
        {
            _type = OperatorType::Constant;
            _constant_value = environment.randomiser().NextDouble(environment.constant_minimum, environment.constant_maximum);
        }
    }
    else
    {
        // Use the operator
        _type = OperatorType::Function;
        _function_operator = &environment.operators.SelectAtRandom(environment.randomiser());
        _input_index = 0;
        _number_of_children = _function_operator->number_of_inputs;
    }

    //
    // Delete old children
    //
    if (old_children == 2 && _number_of_children == 1)
    {
        // Randomise order
        if (environment.randomiser().NextInteger(2) == 0)
        {
            Program* child0 = _children[0];
            _children[0] = _children[1];
            _children[1] = child0;
        }
    }

    for(int n = _number_of_children; n < old_children; n++)
    {
        delete _children[n];
        _children[n] = nullptr;
    }

    // Create new children
    for(int n = old_children; n < _number_of_children; n++)
    {
        ASSERT(_depth != environment.tree_depth);
        _children[n] = new Program(environment, this, n, _depth + 1);
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Evaluates a program against the specified observations                      
/// Returns the raw fitness.                                                    
///                                                                             
///-----------------------------------------------------------------------------
double Program::EvaluateAll(const Environment& environment, const DataSet& dataset)
{
    double fitness = 0;

    for(const Observation& observation : dataset.observations)
    {
        double result = Evaluate(observation.data);
        ASSERT(result == result);

        switch(environment.fitness_function)
        {
        case FitnessFunction::classifier:
            if ((int)result != (int)observation.expected)
            {
                // Incorrect
                fitness += 1.0;
            }
            break;
        case FitnessFunction::difference:
            fitness += abs(result - observation.expected);
            break;

        default:
            ASSERT2(false, "environment.fitness_function");
            break;
        }
    }

    return fitness;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Evaluates this node based on the specified input values                     
///                                                                             
///-----------------------------------------------------------------------------
double Program::Evaluate(const std::vector<double>& input_values)
{
    ASSERT(_depth <= 15); // TODO: Remove this hard-coded value

    // If there is no function to perform we just take an input value
    switch(_type)
    {
    case OperatorType::Constant:
        ASSERT(_function_operator == NULL);
        return _constant_value;

    case OperatorType::Variable:
        ASSERT(_function_operator == NULL);
        return input_values[_input_index];

    case OperatorType::Function:
        {
            // Get the values from the children
            double child_results[2];

            for(int n = 0; n < _number_of_children; n++)
            {
                child_results[n] = _children[n]->Evaluate(input_values);
            }

            // Pass them to our intended function
            double result = (_function_operator->function_pointer)(child_results);
            return Utilities::ConstrainValue(result);
        }

    default:
        ASSERT(false);
        return 0.0;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Creates an identical copy of a root-node                                    
///                                                                             
///-----------------------------------------------------------------------------
Program* Program::Clone(const Program& parent1)
{
    ASSERT(parent1._parent == nullptr);
    Program* result = new Program(parent1, nullptr, 0);

    // Since an exact copy has been made the fitness can be kept
    if (parent1.HasFitness())
    {
        result->SetFitness(parent1.GetFitness());
    }

    return result;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Breeds two root-nodes                                                       
///                                                                             
///---------------------------------------------------------------------------- 
void Program::Breed(const Environment& environment, Program& parent1, Program& parent2, Program** child1, Program** child2)
{
    ASSERT(parent1._parent == nullptr);
    ASSERT(parent2._parent == nullptr);

    Program* result1 = new Program(parent1, nullptr, 0);
    Program* result2 = new Program(parent2, nullptr, 0);

    Program* random1;
    Program* random2;

    do
    {
        random1 = &(result1->PickRandomNode(environment));
        random2 = &(result2->PickRandomNode(environment));
    } 
    while ((random1->GetHeight() + random2->_depth - 1) >= environment.tree_depth
        || (random2->GetHeight() + random1->_depth - 1) >= environment.tree_depth);

    int random1_depth = random1->_depth;
    Program* random1_parent = random1->_parent;
    int random1_index = random1->_index_in_parent;

    if (random1->_parent)
    {
        random1->_parent->_children[random1->_index_in_parent] = random2;
    }
    else
    {
        result1 = random2;
    }

    if (random2->_parent)
    {
        random2->_parent->_children[random2->_index_in_parent] = random1;
    }
    else
    {
        result2 = random1;
    }

    random1->_depth = random2->_depth;
    random1->_parent = random2->_parent;
    random1->_index_in_parent = random2->_index_in_parent;

    random2->_depth = random1_depth;
    random2->_parent = random1_parent;
    random2->_index_in_parent = random1_index;

    ASSERT(result1->_parent == nullptr);
    ASSERT(result2->_parent == nullptr);
    ASSERT(!result1->_has_fitness);
    ASSERT(!result2->_has_fitness);

    *child1 = result1;
    *child2 = result2;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Mutates a root-node                                                         
///                                                                             
///-----------------------------------------------------------------------------
Program* Program::Mutate(const Environment& environment, const Program& parent1)
{
    ASSERT(parent1._parent == nullptr);

    Program* result = new Program(parent1, nullptr, 0);

    Program* mutation_point = &(result->PickRandomNode(environment));
    mutation_point->PickRandomOperator(environment);

    ASSERT(result->_parent == nullptr);
    ASSERT(!result->_has_fitness);
    return result;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Picks a random child node                                                   
///                                                                             
///-----------------------------------------------------------------------------
Program& Program::PickRandomNode(const Environment& environment)
{
    std::vector<Program*> all_nodes;   // TODO: Could probably be more efficient than this

    ListAllNodes(all_nodes);

    ASSERT(all_nodes.size() != 0);

    return *all_nodes[environment.randomiser().NextInteger(static_cast<int>(all_nodes.size()))];
}

///-----------------------------------------------------------------------------
///                                                                             
/// Adds all child nodes and their sub-nodes to a vector                        
///                                                                             
///-----------------------------------------------------------------------------
void Program::ListAllNodes(std::vector<Program*>& all_nodes)
{
    all_nodes.push_back(this);

    for(int n = 0; n < _number_of_children; n++)
    {
        _children[n]->ListAllNodes(all_nodes);
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Height of this node                                                         
///                                                                             
///-----------------------------------------------------------------------------
int Program::GetHeight() const
{
    int height = 1;

    for(int n = 0; n < _number_of_children; n++)
    {
        int child_height = _children[n]->GetHeight() + 1;

        if (child_height > height)
        {
            height = child_height;
        }
    }

    return height;
}

///----------------------------------------------------------------------------
///                                                                            
/// Displays this node as text                                                 
///                                                                            
///----------------------------------------------------------------------------
void Program::ToString(std::ostream& output, int indent) const
{
    const bool colour = &output == &std::cout;

    if (indent > 0)
    {
        if (colour) output << ConsoleColour::LightGray;
        output << std::string(indent, ',');
    }

    switch(_type)
    {
    case OperatorType::Constant:
        ASSERT(_function_operator == NULL);
        if (colour) output << ConsoleColour::DarkGray;
        output << "C:";
        if (colour) output << ConsoleColour::LightGray;
        output << _constant_value;
        if (indent >= 0) output << std::endl;
        break;

    case OperatorType::Variable:
        ASSERT(_function_operator == NULL);
        if (colour) output << ConsoleColour::DarkCyan;
        output << "V:";
        if (colour) output << ConsoleColour::Cyan;
        output << _input_index;
        if (indent >= 0) output << std::endl;
        break;

    case OperatorType::Function:
        if (colour) output << ConsoleColour::Green;
        output << _function_operator->name;
        if (indent >= 0)
        {
            output << std::endl;
        }
        else
        {
            if (colour) output << ConsoleColour::LightGray;
            output << "( ";
        }
        int next_indent = (indent >= 0) ? (indent + 1) : indent;

        for(int n = 0; n < _number_of_children; n++)
        {
            if (indent < 0 && n != 0)
            {
                if (colour) output << ConsoleColour::LightGray;
                output << ", ";
            }

            _children[n]->ToString(output, next_indent);
        };

        if (indent < 0)
        {
            if (colour) output << ConsoleColour::LightGray;
            output << " )";
        }
        break;
    }

    if (colour) output << ConsoleColour::Reset;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Displays this node as text (no children)                                    
///                                                                             
///-----------------------------------------------------------------------------
void Program::Name(std::ostream& output) const
{    
    switch(_type)
    {
    case OperatorType::Constant:
        ASSERT(_function_operator == NULL);
        output << "C:" << _constant_value;
        return;

    case OperatorType::Variable:
        ASSERT(_function_operator == NULL);
        output << "V:" << _input_index;
        return;

    case OperatorType::Function:
        output << _function_operator->name;
        return;
    }
}

///-----------------------------------------------------------------------------
///                                                                             
/// Whether this program has been evaluated for fitness                         
///                                                                             
///-----------------------------------------------------------------------------
bool Program::HasFitness() const
{
    // This should never be asked for a non-root program
    ASSERT(_depth == 0);

    return _has_fitness;
}

///-----------------------------------------------------------------------------
///                                                                             
/// The fitness value for this program.                                         
///                                                                             
///-----------------------------------------------------------------------------
double Program::GetFitness() const
{
    // A fitness value must be present
    ASSERT(_has_fitness);

    return _fitness;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Sets the fitness value for this program.                                    
///                                                                             
///-----------------------------------------------------------------------------
void Program::SetFitness(double value)
{
    // This must be a root program
    ASSERT(_depth == 0);

    // A fitness value must not have already been assigned
    ASSERT(!_has_fitness);

    // Assign fitness and mark as set
    _has_fitness = true;
    _fitness = value;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Returns variables encountered in this program                               
///                                                                             
///-----------------------------------------------------------------------------
void Program::GetCommonVariables(std::map<int, int>& map)
{
    // Iterate through all children
    for(int n = 0; n < _number_of_children; n++)
    {
        _children[n]->GetCommonVariables(map);
    }

    // Check this program
    if (_type == OperatorType::Variable)
    {
        ASSERT(_function_operator == nullptr);
        map[_input_index]++;
    }
}
