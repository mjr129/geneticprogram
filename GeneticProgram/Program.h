#pragma once

#include "FunctionOperator.h"
#include <vector>
#include <map>

class Environment;
class DataSet;

namespace OperatorType
{
    enum Type
    {
        Function,
        Variable,
        Constant
    };
};

class Program
{
private:
	Program* _parent; // unowned
    int _index_in_parent;
    OperatorType::Type _type;
	const FunctionOperator* _function_operator; // unowned
	int _input_index;
    double _constant_value;
	int _number_of_children;
    int _depth;
    static const int _max_children = 2;
	Program* _children[_max_children]; // owned
    double _fitness;
    bool _has_fitness;

public:
	Program(const Program& copyFrom, Program* parent, int index_in_parent);
	Program(const Environment& environment, Program* parent, int index_in_parent, int depth);
    Program(const Environment& environment, Program* parent, std::string& text, int& index, int index_in_parent);
    ~Program();
    double EvaluateAll(const Environment& environment, const DataSet& dataset);
	double Evaluate(const std::vector<double>& input_values);

    static Program* Clone(const Program& a);
    static void Breed(const Environment& environment, Program& a, Program& b, Program** out1, Program** out2);
    static Program* Mutate(const Environment& environment, const Program& a);

    Program& PickRandomNode(const Environment& environment);
    void ListAllNodes(std::vector<Program*>& all_nodes);
    void ToString(std::ostream& output, int indent) const;
    void Name(std::ostream& output) const;
    int GetHeight() const;

    bool HasFitness() const;
    double GetFitness() const;
    void SetFitness(double value);
    void GetCommonVariables(std::map<int, int>& map);

private:
    void PickRandomOperator(const Environment& environment);
};
