#include "Common.h"
#include "Randomiser.h"

#include <random>
#include <ctime>

///-----------------------------------------------------------------------------
///                                                                             
/// Constructor                                                                 
///                                                                             
///-----------------------------------------------------------------------------
Randomiser::Randomiser()
    : engine( clock() )
{
    // No further action
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates a random double between 0 (inclusive) and 1 (exclusive)           
///                                                                             
///-----------------------------------------------------------------------------
double Randomiser::NextDouble()
{
    std::uniform_real_distribution<> distribution(0.0, 1.0);
    return distribution(engine);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates a random double between 0 (inclusive) and maximum (exclusive)     
///                                                                             
///-----------------------------------------------------------------------------
double Randomiser::NextDouble(double maximum)
{
    std::uniform_real_distribution<> distribution(0.0, maximum);
    return distribution(engine);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates a random double between minimum (inclusive)                       
/// and maximum (exclusive)                                                     
///                                                                             
///-----------------------------------------------------------------------------
double Randomiser::NextDouble(double minimum, double maximum)
{
    std::uniform_real_distribution<> distribution(minimum, maximum);
    return distribution(engine);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates a random boolean value                                            
///                                                                             
///-----------------------------------------------------------------------------
bool Randomiser::NextBoolean()
{
    std::uniform_int_distribution<> distribution(0, 1);
    return distribution(engine) == 0;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates a random integer between 0 (inclusive) and maximum (exclusive)    
///                                                                             
///-----------------------------------------------------------------------------
int Randomiser::NextInteger(int maximum)
{
    std::uniform_int_distribution<> distribution(0, maximum - 1);
    return distribution(engine);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Generates a random integer between minimum (inclusive)                      
/// and maximum (exclusive)                                                     
///                                                                             
///-----------------------------------------------------------------------------
int Randomiser::NextInteger(int minimum, int maximum)
{
    std::uniform_int_distribution<> distribution(minimum, maximum - 1);
    return distribution(engine);
}

///-----------------------------------------------------------------------------
///                                                                              
/// Name of the randomiser engine.                                              
///                                                                             
///-----------------------------------------------------------------------------
const char* Randomiser::Name()
{
    return STRINGIFY(RANDOMISER_ENGINE);
}
