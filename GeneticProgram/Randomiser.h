#pragma once

#include <random>

#define RANDOMISER_ENGINE std::mt19937
// #define RANDOMISER_ENGINE std::minstd_rand

///-----------------------------------------------------------------------------
///                                                                             
/// Randomiser                                                                  
///                                                                             
/// Currently set to use the Mersenne Twister 19937 random number generator.    
///                                                                             
///-----------------------------------------------------------------------------
class Randomiser
{
private:
	/// Engine
    RANDOMISER_ENGINE engine;

public:
    Randomiser();

	// Doubles
	double NextDouble();
	double NextDouble(double maximum);
	double NextDouble(double minimum, double maximum);

	// Integers
	int NextInteger(int maximum);
	int NextInteger(int minimum, int maximum);

    // Boolean
    bool NextBoolean();

    // Name of randomiser
    const char* Name();
};
