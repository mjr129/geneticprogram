#include "Common.h"
#include "ScriptReader.h"
#include "Environment.h"
#include "World.h"
#include "Utilities.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#define SIMPLE_COMMAND(x,y,z)     else if ((elements[0] == #x || elements[0] == y) && elements.size() == 2) { std::cout << "<SCRIPT> SET " #x << " = " << elements[1].c_str() << std::endl; environment.##x = z; }
#define SIMPLE_STRING_COMMAND(x,y) SIMPLE_COMMAND(x, y, elements[1])
#define SIMPLE_INT_COMMAND(x,y)    SIMPLE_COMMAND(x, y, atoi(elements[1].c_str()))
#define SIMPLE_ENUM_COMMAND(x,y,t) SIMPLE_COMMAND(x, y, Utilities::StringToEnum<t>(elements[1]))
#define SIMPLE_REAL_COMMAND(x,y)   SIMPLE_COMMAND(x, y, atof(elements[1].c_str()))

/// ----------------------------------------------------------------------------
///                                                                             
/// Reads and executes the settings file.                                       
///                                                                             
/// This sets up the environment in which the program runs.                     
/// See Environment::Process for the actual core of this application.           
///                                                                             
/// @param text The settings file to read in.                                   
///                                                                             
/// ----------------------------------------------------------------------------
void ScriptReader::Interpret(const char* text)
{
    Environment environment;
    int index = 0;

    while(text == nullptr || text[index] != '\0')
    {
        std::vector<std::string> elements;

        if (text == nullptr)
        {
            std::cout << "<INPUT> ";
            std::string in_line;
            std::getline(std::cin, in_line);
            index = 0;
            elements = Utilities::Split(in_line.c_str(), index);
        }
        else
        {
            elements = Utilities::Split(text, index);
        }

        if (elements.size() != 0)
        {
            if (elements[0] == "process" && elements.size() == 1)
            {
                // EXECUTE PROGRAM
                std::cout << "<SCRIPT> PROCESS" << std::endl;
                environment.Process();
            }
            else if (elements[0] == "system" && elements.size() == 2)
            {
                // RUN A SYSTEM COMMAND
                std::cout << "<SCRIPT> SYSTEM " << elements[1].c_str() << std::endl;
                system(elements[1].c_str());
            }
            else if (elements[0] == "echo" && elements.size() == 2)
            {
                // PRINT SOME TEXT
                std::cout << "<SCRIPT> ECHO ";
                std::cout << elements[1].c_str() << std::endl;
            }
            else if (elements[0] == "pause" && elements.size() == 1)
            {
                // PAUSE THE DISPLAY
                std::cout << "<SCRIPT> PAUSE " << std::endl;
                system("pause");
            }
            else if (elements[0] == "#" || elements[0] == "")
            {
                // COMMENT STATEMENT
                // std::cout << "<SCRIPT> COMMENT" << std::endl;
            }
            else if ((elements[0] == "source_data") && elements.size() == 2)
            {
                // LOAD SOME SOURCE DATA
                environment.source_data.LoadFile(elements[1].c_str());
                std::cout << "<SCRIPT> SET source data " << elements[1].c_str() << " [" << environment.source_data.observations.size() << "x" << environment.source_data.variable_count() << "]" << std::endl;
            }
            else if ((elements[0] == "data") && elements.size() == 2)
            {
                // LOAD SOME DATA
                environment.data.LoadFile(elements[1].c_str());
                std::cout << "<SCRIPT> SET data " << elements[1].c_str() << " [" << environment.data.observations.size() << "x" << environment.data.variable_count() << "]" << std::endl;
            }
            else if (elements[0] == "validation_data" && elements.size() == 2)
            {
                // LOAD SOME VALIDATION DATA
                environment.validation_data.LoadFile(elements[1].c_str());
                std::cout << "<SCRIPT> SET validation_data " << elements[1].c_str() << " [" << environment.validation_data.observations.size() << "x" << environment.validation_data.variable_count() << "]" << std::endl;
            }
            else if (elements[0] == "operators" && elements.size() == 2)
            {
                // LOAD THE OPERATORS
                environment.operators.PopulateFromList(elements[1]);
                std::cout << "<SCRIPT> SET operators " << environment.operators.ToString().c_str() << std::endl;
            }
            SIMPLE_STRING_COMMAND(description, "description")
            SIMPLE_INT_COMMAND(iteration, "iteration")
            SIMPLE_INT_COMMAND(iterations, "iterations")
            SIMPLE_INT_COMMAND(population_size, "population")
            SIMPLE_INT_COMMAND(tree_depth, "tree")
            SIMPLE_REAL_COMMAND(replication_breeding, "replication")
            SIMPLE_REAL_COMMAND(crossover_breeding, "crossover")
            SIMPLE_REAL_COMMAND(mutation_breeding, "mutation")
            SIMPLE_REAL_COMMAND(discard_weakest, "discard")
            SIMPLE_REAL_COMMAND(mutation_aftereffect, "xmutation")
            SIMPLE_REAL_COMMAND(constant_minimum, "cmin")
            SIMPLE_REAL_COMMAND(constant_maximum, "cmax")
            SIMPLE_INT_COMMAND(number_of_populations, "islands")
            SIMPLE_INT_COMMAND(migration_interval, "interval")
            SIMPLE_REAL_COMMAND(migration_quantity, "migration")
            SIMPLE_INT_COMMAND(stop_after_generations, "maxgen")
            SIMPLE_REAL_COMMAND(stop_after_matches, "maxmatch")
            SIMPLE_INT_COMMAND(stop_after_unchanging, "unchanging")
            SIMPLE_REAL_COMMAND(two_stage_matches, "twostage")
            SIMPLE_INT_COMMAND(two_stage_variable_range, "twostagerange")
            SIMPLE_STRING_COMMAND(output_directory, "output")
            SIMPLE_STRING_COMMAND(program_file, "program")
            SIMPLE_REAL_COMMAND(common_variable_listing, "variablelisting")
            SIMPLE_ENUM_COMMAND(mode, "mode", ProcessingMode)
            SIMPLE_ENUM_COMMAND(fitness_function, "fitness", FitnessFunction)
            SIMPLE_ENUM_COMMAND(selection_method, "selection", SelectionMethod)
            SIMPLE_REAL_COMMAND(training_fraction, "training_fraction")
            else
            {
                std::cout << "<SCRIPT> Unrecognised command or wrong number of arguments: " << elements[0].c_str() << " (" << (elements.size() - 1) << ")" << std::endl;
                ASSERT2(false, "Unrecognised script command, check above.");
            }
        }
    }
}