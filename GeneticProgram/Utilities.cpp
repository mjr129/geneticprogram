#include "Common.h"
#include "Utilities.h"

#include <fstream>
#include <vector>
#include <sstream>

/// ----------------------------------------------------------------------------
///                                                                             
/// Reads a file to a string.                                                   
///                                                                             
/// ----------------------------------------------------------------------------
std::string Utilities::ReadFileAsText(const char* file_name)
{
    std::ifstream in(file_name, std::ios::in | std::ios::binary);
    if (in.fail())
    {
        ASSERT2(!in.fail(), file_name);
        return std::string("");
    }
    in.seekg(0, std::ios::end);
    int file_size = static_cast<int>(in.tellg());
    char* file_text = new char[file_size + 1];
    file_text[file_size] = '\0';
    in.seekg(0, std::ios::beg);
    in.read(file_text, file_size);
    in.close();
    std::string result(file_text);
    delete file_text;
    return result;
}

/// ----------------------------------------------------------------------------
///                                                                             
/// Tokenises a string.                                                         
///                                                                             
/// ----------------------------------------------------------------------------
bool Utilities::Tokenise(const std::string& text, const std::string& tokens, size_t& seek_position, std::string& result)
{
    if (seek_position >= text.size())
    {
        return false;
    }

    size_t next;

    do
    {
        size_t previous = seek_position;
        next = text.find_first_of(tokens.c_str(), seek_position);

        if (next == std::string::npos)
        {
            next = text.size();
        }

        seek_position = next + 1;

        if (next != previous)
        {
            result = text.substr(previous, next - previous);

            return true;
        }
    } while (next != text.size());

    return false;
}

/// ----------------------------------------------------------------------------
///                                                                             
/// Constrains a value to the valid range of real numbers.                      
///                                                                             
/// ----------------------------------------------------------------------------
double Utilities::ConstrainValue(double value)
{
    // Bad values
    if (value > DBL_MAX)
    {
        // +inf
        return DBL_MAX;
    }
    else if (value < -DBL_MAX)
    {
        // -inf
        return -DBL_MAX;
    }
    else if (value != value)
    {
        // nan
        return 0.0;
    }

    return value;
}

/// ----------------------------------------------------------------------------
///                                                                             
/// Splits a string about spaces, accounting for quotes.                        
/// Could probably be incorporated into Tokenise.                               
///                                                                             
/// ----------------------------------------------------------------------------
std::vector<std::string> Utilities::Split(const char* text, int& index)
{
    std::vector<std::string> result;
    std::stringstream current;
    bool in_quotes = false;
    bool empty = false;

    for(; text[index] != '\0'; index++)
    {
        if (text[index] == '\r')
        {
            continue;
        }
        else if (text[index] == '\n')
        {
            index++;
            break;
        }   
        else if (text[index] == '"')
        {
            in_quotes = !in_quotes;
            if (in_quotes)
            {
                empty = false;
            }
        }
        else if (!in_quotes && (text[index] == ' ' || text[index] == '\t'))
        {
            if (!empty)
            {
                result.push_back(current.str());
                current.str("");
                empty = true;
            }
        }
        else
        {
            current << text[index];
            empty = false;
        }
    }

    if (!empty)
    {
        result.push_back(current.str());
        current.str("");
        empty = true;
    }

    return result;
}
