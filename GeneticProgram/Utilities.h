#pragma once

#include <string>
#include <vector>

namespace Utilities
{
    std::string ReadFileAsText(const char* file_name);
    bool Tokenise(const std::string& text, const std::string& tokens, size_t& seek_position, std::string& result);
    double ConstrainValue(double value);
    std::vector<std::string> Split(const char* text, int& index);
    template<typename T> typename T::Type StringToEnum(std::string text);
    template<typename T> const char* EnumToString(typename T::Type value);
};

// Inline stuff

template<typename T>
typename T::Type Utilities::StringToEnum(std::string text)
{
    for(int index = 0; index < T::Length; index++)
    {
        if (text == std::string(T::Names[index]))
        {
            return static_cast<T::Type>(index);
        }
    }

    ASSERT2(false, (text + " is an invalid enum value.").c_str());
    return static_cast<T::Type>(-1);
}

template<typename T>
const char* Utilities::EnumToString(typename T::Type value)
{
    int ivalue = static_cast<int>(value);

    if (ivalue >= 0 && ivalue < T::Length)
    {
        return T::Names[ivalue];
    }

    return "Unknown";
}