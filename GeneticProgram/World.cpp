#include "Common.h"
#include "World.h"

#include "Program.h"
#include "Population.h"
#include "EvaluationResult.h"
#include "FileWriter.h"
#include "ProcessResult.h"

#include <iostream>
#include <vector>
#include <thread>
#include <future>
#include <sstream>

///----------------------------------------------------------------------------
///                                                                            
/// Constructor                                                                
///                                                                            
/// Initialises all the populations                                            
///                                                                            
///----------------------------------------------------------------------------
World::World(const Environment& environment)
    : _environment(environment)
    , _generation(0)
    , _stage(ProcessingStage::normal)
{
    ResetPopulations();
    _stage = ProcessingStage::normal;
}

///----------------------------------------------------------------------------
///                                                                            
/// Resets all populations to random content                                   
///                                                                            
///----------------------------------------------------------------------------
void World::ResetPopulations()
{
    //
    // Delete old populations
    //
    for(Population* population : _populations)
    {
        delete population;
    }

    _populations.clear();

    //
    // Add new populations
    //
    for(int n = 0; n < _environment.number_of_populations; n++)
    {
        _populations.push_back( new Population( _environment ) );
    }

    ASSERT(_stage == ProcessingStage::normal);
    _stage = ProcessingStage::second;
}

///----------------------------------------------------------------------------
///                                                                            
/// Destructor                                                                 
///                                                                            
///----------------------------------------------------------------------------
World::~World()
{
    for(Population* population : _populations)
    {
        delete population;
    }
}

///----------------------------------------------------------------------------
///                                                                            
/// Process all populations                                                    
///                                                                            
/// @return Whether to continue processing                                     
///         (i.e. true until a stopping condition is met).                     
///                                                                            
///----------------------------------------------------------------------------
ProcessResult World::Process()
{
    Program* best_result = nullptr;

    /////////////////////////
    // Evaluate populations
    //
    std::vector<std::future<Program*>> threads;

    for(Population* population : _populations)
    {
        //
        // Process each island in a different thread
        //
        DPF("World::Process: Process population...");
        ASSERT(&(population->_environment) == &_environment);
        threads.push_back( std::async( [] (Population* population) { return population->Evaluate(); }, population) );

        //
        // If debugging then wait for threads before starting the next so as
        // not to clutter console output.
        //
#ifdef VERBOSE_DEBUG
        threads.back().wait();
#endif
    }

    //
    // Is this a migration round?
    //
    bool migrate = (_environment.migration_interval != 0) 
                && (_generation != 0)
                && ((_generation % _environment.migration_interval) == 0);
#ifdef VERBOSE_DEBUG
    if (migrate) DPF("World::Process: Migration round...");
#endif

    //
    // Find the best result from all the threads
    //
    for(std::future<Program*>& future: threads)
    {
        Program* result = future.get();

        // Lower raw fitness is better
        if (best_result == nullptr || result->GetFitness() < best_result->GetFitness())
        {
            best_result = result;
        }
    }

    //
    // Also get the validation fitness from the best result
    // (for curiosity only - unseen data obviously cannot be used for selection).
    //
    double data_fitness = best_result->GetFitness();
    double validation_fitness = best_result->EvaluateAll( _environment, _environment.validation_data );

    if (data_fitness == _last_fitness)
    {
        _unchanged_generations++;
    }
    else
    {
        _last_fitness = data_fitness;
        _unchanged_generations = 0;
    }

    //////////////////////////////////
    // Check the stopping conditions
    //
    StopReason::Type stopreason;

    if (_generation >= _environment.stop_after_generations)
    {
        stopreason = StopReason::Generation;
    }
    else if (_unchanged_generations >= _environment.stop_after_unchanging)
    {
        stopreason = StopReason::Unchanging;
    }
    else if ( (_environment.fitness_function == FitnessFunction::classifier) &&
            ( (1.0 - (best_result->GetFitness() / _environment.data.observations.size())) >= _environment.stop_after_matches) )
    {
        stopreason = StopReason::Matched;
    }
    else if ( (_environment.fitness_function == FitnessFunction::difference) &&
            ( (best_result->GetFitness() / _environment.data.observations.size()) < _environment.stop_after_matches) )
    {
        stopreason = StopReason::Matched;
    }
    else if (_stage == ProcessingStage::normal && _environment.two_stage_matches)
    {
        switch(_environment.fitness_function)
        {
        case FitnessFunction::classifier:
            stopreason = (1 - (data_fitness / _environment.data.observations.size()) >= _environment.two_stage_matches)
                ? StopReason::TwoStage1 : StopReason::None;
                break;

        case FitnessFunction::difference:
            stopreason = ((data_fitness / _environment.data.observations.size()) <= _environment.two_stage_matches)
                ? StopReason::TwoStage1 : StopReason::None;
            break;

        default:
            ASSERT2(false, "Invalid switch on _environment.fitness_function");
            stopreason = StopReason::Generation;
            break;
        }
    }
    else
    {
        stopreason = StopReason::None;
    }

    if (stopreason != StopReason::None)
    {
        DPF("World::Process: Stop!");
        _generation++;
        return ProcessResult(_generation - 1, data_fitness, validation_fitness, best_result, GetCommonVariables(_environment.common_variable_listing), stopreason);
    }

    //////////////////////////////////////////
    // If it's time then migrate individuals
    //
    if (migrate)
    {
        for(uint index = 0; index < _populations.size(); index++)
        {
            int adj_index = (index + 1) % _populations.size();
            DPF("World::Process: Migrate population %d into %d...", index, adj_index);
            _populations[index]->MigrateInto(*_populations[adj_index]);
        }
    }

    ///////////////////////////////
    // Create the next generation
    //
    std::vector<std::future<void>> threads2;

    for(Population* population : _populations)
    {
        DPF("World::Process: Create next generation for population...");
        threads2.push_back( std::async( [] (Population* population) { population->NextGeneration(); }, population) );

#ifdef VERBOSE_DEBUG
        threads2.back().wait();
#endif
    }

    for(std::future<void>& future: threads2)
    {
        future.wait();
    }

    /////////////////////////////////////////
    // Display some information to the user
    //
    _generation++;

    return ProcessResult(_generation - 1, data_fitness, validation_fitness);
}

///-----------------------------------------------------------------------------
///                                                                             
/// Returns variables encountered in the top 10% of programs                    
///                                                                             
///-----------------------------------------------------------------------------
std::map<int, int> World::GetCommonVariables(double fraction)
{
    std::map<int, int> result;

    for(Population* population : _populations)
    {
        population->GetCommonVariables(fraction, result);
    }

    return result;
}

///-----------------------------------------------------------------------------
///                                                                             
/// Writes all the programs into a file                                         
///                                                                             
///-----------------------------------------------------------------------------
void World::WriteAllPrograms()
{
    for(size_t pi = 0; pi < _populations.size(); pi++)
    {
        Population* population = _populations[pi];
        for(size_t i = 0; i < population->_programs.size(); i++)
        {
            Program* program = population->_programs[i];
            std::stringstream ss;
            program->ToString(ss, -1);

            _environment.Output(FileId::AllPrograms, "%d, %d, %f, \"%s\"", pi, i, program->GetFitness(), ss.str().c_str()); 
        }
    }
}
