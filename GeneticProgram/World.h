#pragma once


#include "Environment.h"
#include <vector>
#include <map>

class Population;
class Program;
class ProcessResult;

///-----------------------------------------------------------------------------
///                                                                             
/// Represents the set of demetic populations                                   
///                                                                             
/// Instantiated and destroyed through the master command "ProcessToEnd", which 
/// does pretty much everything.                                                
///                                                                             
///-----------------------------------------------------------------------------
class World
{
private:
    // Variables
    const Environment& _environment;
    std::vector<Population*> _populations;
    int _generation;
    int _unchanged_generations;
    double _last_fitness;
    ProcessingStage::Type _stage;

public:
    // Constructors
    World(const Environment& environment);
    DISALLOW_COPY(World);
    ~World();

    // Functions
    void ResetPopulations();
    ProcessResult Process();
    void WriteAllPrograms();

private:
    std::map<int, int> GetCommonVariables(double fraction);
};
