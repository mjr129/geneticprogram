#include "Common.h"
#include "World.h"
#include "Environment.h"
#include "Randomiser.h"
#include "OperatorBag.h"
#include "Program.h"
#include "FileWriter.h"
#include "ScriptReader.h"
#include "Utilities.h"

#include <iostream>
#include <fstream>
#include <ctime>
#include <sstream>
#include <cstdarg>

///----------------------------------------------------------------------------
///                                                                            
/// Entry point                                                                
///                                                                            
/// For the main program functionality see ScriptReader::Interpret instead.    
///                                                                            
///----------------------------------------------------------------------------
int main(int argv, const char** args)
{
    // Provide some help if a file has not been specified
    if (argv != 2)
    {
        std::cout << "Usage:" << std::endl;
        std::cout << "    GeneticProgram.exe <SCRIPT_FILE>" << std::endl;
        std::cout << std::endl;
        std::cout << "        <SCRIPT_FILE> - name of file to run." << std::endl;
        std::cout << "                        see \"test.txt\" for an example." << std::endl;
    }

    // Now either read from stdin or a file, depending on whether a file is provided
    if (argv == 1)
    {
        // No file, use stdin
        std::cout << "<COMMAND> READ FROM STANDARD INPUT" << std::endl;
        ScriptReader::Interpret(nullptr);
    }
    else if (argv == 2)
    {
        // The command line represents a file-name, so read in that file
        std::cout << "<COMMAND> " << args[1] << std::endl;
        std::string script = Utilities::ReadFileAsText(args[1]);

        // Execute the script
        ScriptReader::Interpret(script.c_str());
    }
    else
    {
         std::cout << "<COMMAND> UNRECOGNISED COMMAND LINE ARGUMENTS" << std::endl;
    }
}

///----------------------------------------------------------------------------
///                                                                            
/// Assertion function.                                                        
///                                                                            
///----------------------------------------------------------------------------
void my_assert(bool condition, const char* expression, const char* message, ...)
{
    if (!condition)
    {
        std::cout << "ASSERT FAILED:" << std::endl;
        std::cout << "MESSAGE: ";
        va_list args;
        va_start(args, message);
        vprintf(message, args);
        va_end (args);
        std::cout << std::endl;
        std::cout << "EXPRESSION: " << expression << std::endl;
        for(;;)
        {
            std::cout << "[I]gnore [D]ebug [E]xit" << std::endl;
            std::cout << "> ";
            std::string in_line;
            std::getline(std::cin, in_line);

            if (in_line.size() == 0)
            {
                continue;
            }

            switch(in_line[0])
            {
                case 'I':
                case 'i':
                    std::cout << "IGNORE" << std::endl;
                    return;
                case 'D':
                case 'd':
                    std::cout << "DEBUG" << std::endl;
                    __debugbreak();
                    return;
                case 'E':
                case 'e':
                    std::cout << "EXIT" << std::endl;
                    exit(1);
                    return;
            }
        }
    }
}