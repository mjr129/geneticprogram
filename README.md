Genetic Program
===============

A genetic program application.

Installation
------------

Via source code only: Open the SLN file in Visual Studio and press run.

Meta
----

```ini
type=application
language=C++
host=bitbucket
```